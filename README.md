# README #

A-Team LTRAS Farming Operations App

### What is this repository for? ###

* ECS193B

### How do I get set up? ###

* Download source
* Build iOS app from ...
* Add <source>/www/ to LAMP/WAMP/XAMPP server DocumentRoot

### Bugs ###
* Please report bugs to the Bitbucket bug tracker.