<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Add Equipment</title>

    <!-- Bootstrap core CSS -->
    <link href="includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">

  </head>
<body>
    <div class="container">
        <?php include ('includes/navigationbar.php'); ?>

        <!--page 1-->
        <form role="form" action="public/webapp_add_equipment.php" method="post">
            <h2>Please Enter Information for the New Equipment:</h2>
            <div class="form-group">
                <label for="EquipmentName">Equipment Name:</label>
                <input type="text" class="form-control" id="EquipmentName" name="EquipmentName" placeholder="Enter Equipment Name">
            </div>
            <div class="form-group">
                <label for="EquipmentWidth">Width (ft):</label>
                <input type="number" step="any" class="form-control" id="EquipmentWidth" name="EquipmentWidth">
            </div>
            <div class="form-group">
                <label for="EquipmentDescription">Description:</label>
                <input type="text" class="form-control" id="EquipmentDescription" name="EquipmentDescription">
            </div>
            <div class="form-group">
                <label for="EquipmentComments">Comments:</label>
                <textarea type="text" class="form-control" id="EquipmentComments" name="EquipmentComments" rows="3"></textarea>
            </div>
			<!--Not visible on equipment table, are these columns necessary anymore?
            <h4>Wasn't on iOS form but are on table</h4>
            <div class="form-group">
                <label for="ASAENameMatch">Name Match From ASAE:</label>
                <input type="text" class="form-control" id="ASAENameMatch" name="ASAENameMatch">
            </div>
            <div class="form-group">
                <label for="SpeedLowerLimit">Speed (lower limit):</label>
                <input type="number" step="any" class="form-control" id="SpeedLowerLimit" name="SpeedLowerLimit">
            </div>
            <div class="form-group">
                <label for="SpeedTypical">Speed (typical):</label>
                <input type="number" step="any" class="form-control" id="SpeedTypical" name="SpeedTypical">
            </div>
            <div class="form-group">
                <label for="EfficiencyLowerLimit">Efficiency (lower limit):</label>
                <input type="number" class="form-control" id="EfficiencyLowerLimit" name="EfficiencyLowerLimit">
            </div>
            <div class="form-group">
                <label for="EfficiencyTypical">Efficiency (typical):</label>
                <input type="number" class="form-control" id="EfficiencyTypical" name="EfficiencyTypical">
            </div>
            <div class="form-group">
                <label for="EquipmentEstimatedFarmHours">Estimated Farm Hours/Acre:</label>
                <input type="text" class="form-control" id="EquipmentEstimatedFarmHours" name="EquipmentEstimatedFarmHours">
            </div>
            <div class="form-group">
                <label for="EquipmentEstimatedExperimentHours">Estimated Experiment Hours/Acre:</label>
                <input type="text" class="form-control" id="EquipmentEstimatedExperimentHours" name="EquipmentEstimatedExperimentHours">
            </div>
            <div class="form-group">
                <label for="EquipmentTheoreticalHours">Theoretical Hours/Acre:</label>
                <input type="text" class="form-control" id="EquipmentTheoreticalHours" name="EquipmentTheoreticalHours">
            </div>
            -->
            <button class="btn btn-lg btn-primary btn-block" type="submit">Add Equipment</button>
        </form>
      </div>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
