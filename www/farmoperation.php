<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>Farm Operations Form</title>

        <!-- Bootstrap core CSS -->
        <link href="includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="navbar" rel="stylesheet">
        
        <!--datepicker CSS -->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" rel="stylesheet">
        <!--<link href="includes/datepicker/css/datepicker.css" rel="stylesheet">-->
    </head>
    <body>
        <div class="container">
            <?php
                include ('config.php');
                include_once('includes/navigationbar.php');
                
                $con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
                // Check connection
                if (mysqli_connect_errno()){
                  echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }
                
                //Pull Tractor Description and Tractor ID
                $tractorquery =  "SELECT tractor_id,description FROM tractors ORDER BY description";                               
                $tractorresult = mysqli_query($con, $tractorquery);
                
            ?>
            
            <form role="form" action="public/webapp_add_farm_operation.php" method="post">
                <!--page 1-->
                <h2>Farm Operation Form</h2>
                <div class="form-group">
                    <label for="TractorID">Tractor Description:</label>
                    <?php
                           //Display tractor query output
                            echo '<select class="form-control" id="TractorID" name="TractorID">';
                            while ($row = mysqli_fetch_array($tractorresult, MYSQLI_BOTH)) {
                                 echo '<option value="' . $row{'tractor_id'} . '">' . $row{'description'} . "</option>";
                            }
                            echo '</select>';
                    ?>
                </div>
                
                <div class="form-group">			
                    <p><a href="find_or_add_tractor.php" class="btn btn-default btn-block" role="button">Add Tractor</a></p>
                </div>
                
                <!--page 2-->
                <div class="form-group">
                    <label for="EquipmentID">Equipment Description:</label>
                    <?php
                        //Pull equipment description and tractor_id
                        $equipmentquery =  "SELECT equipment_id,description FROM equipment ORDER BY description";                               
                        $equipmentresult = mysqli_query($con, $equipmentquery);
                
                        echo '<select class="form-control" id="EquipmentID" name="EquipmentID">';
                        while ($row = mysqli_fetch_array($equipmentresult, MYSQLI_BOTH)) {
                             echo '<option value="'. $row{'equipment_id'} . '">' . $row{'description'} . "</option>";
                        }
                        echo '</select>';
                    ?>
                </div>
                
                <div class="form-group">			
                    <p><a href="FindAddEquipment.php" class="btn btn-default btn-block" role="button">Add Equipment</a></p>
                </div>

                <!--page 3-->                
                <!--TODO: autofill date using javascript-->    
                <div class="form-group input-group date">
                    <label for="Date">Date:</label>
                    <input type="datetime" class="form-control" id="Date" name="Date">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
                
                <div class="form-group">
                    <label for="OperationYear">Operation Year:</label>
                    <input type="text" class="form-control" id="OperationYear" name="OperationYear">
                </div>

                <div class="form-group">
                    <label for="OperationType">Operation Type:</label>
                    <?php
                        //Pull operation types 
                        $operationtypequery =  "SELECT operation_type FROM lookup_operation_types ORDER BY operation_type";                               
                        $operationtyperesult = mysqli_query($con, $operationtypequery);
                
                        echo '<select class="form-control" id="OperationType" name="OperationType">';
                        while ($row = mysqli_fetch_array($operationtyperesult, MYSQLI_BOTH)) {
                             echo '<option value="'. $row{'operation_type'} . '">' . $row{'operation_type'} . "</option>";
                        }
                        echo '</select>';
                    ?>
                </div>
            
                <h2>Materials</h2>
                <div class="form-group">
                    <label for="OperationMaterialsID">Material Type:</label>
						<?php
							//Pull materialopid, material, and description from table
							$typequery =  "SELECT operation_materials_id, material, description FROM operation_materials ORDER BY material";                               
							$typeresult = mysqli_query($con, $typequery);
							
							//Dynamically print material types
							echo '<select class="form-control" id="OperationMaterialsID" name="OperationMaterialsID">';
							while ($row = mysqli_fetch_array($typeresult, MYSQLI_BOTH)) {
								 echo '<option value="'. $row{'operation_materials_id'} . '">' . $row{'material'} . " - " . $row{'description'} . "</option>";
							}
							echo '</select>';
						?>
                </div>
                <div class="form-group">
                    <label for="MaterialQuantity">Quantity:</label>
                    <input type="number" class="form-control" id="MaterialQuantity"  name="MaterialQuantity" placeholder="Enter quantity of materials">
                </div>
                <div class="form-group">
                    <label for="MaterialUnits">Units:</label>
                    <?php
                        //Pull material units from lookup_operation_units table
                        $unitsquery =  "SELECT operation_units FROM lookup_operation_units ORDER BY operation_units";                               
                        $unitsresult = mysqli_query($con, $unitsquery);
                        
                        //Dynamically print material units
                        echo '<select class="form-control" id="MaterialUnits" name="MaterialUnits">';
                        while ($unitsrow = mysqli_fetch_array($unitsresult, MYSQLI_BOTH)) {
                             echo '<option>' . $unitsrow{'operation_units'} . "</option>";
                        }
                        echo '</select>';
                    ?>
                </div>
                <div class="form-group">
                    <label for="MaterialComments">Material Comments:*</label>
                    <textarea class="form-control" id="MaterialComments" name="MaterialComments" rows="3"></textarea>
                </div>

                <!--page 4-system-->  
                <h2>Please Pick Your System:</h2>
                <div class="form-group">
                    <label for="SystemNumber">System Name:</label>
                    <?php
                        //Pull system number, name, and code 
                        $systemquery =  "SELECT * FROM system_names ORDER BY system_name";                               
                        $systemresult = mysqli_query($con, $systemquery);
                
                        echo '<select class="form-control" id="SystemNumber" name="SystemNumber">';
                        while ($row = mysqli_fetch_array($systemresult, MYSQLI_BOTH)) {
                             echo '<option value="'. $row{'system_number'} . '">' . $row{'system_name'} . " (" . $row{'system_code'} . ") </option>";
                        }
                        echo '</select>';
                    ?>
                </div>
                
                <h2>Please Pick Your Crop:</h2>
                <!-- need to find the database equivalent/make a table of systems-->
                <div class="form-group">
                    <label for="CropType">Crop Type:</label>
                    <?php
                        //Pull crop type 
                        $cropquery =  "SELECT crop_type FROM lookup_crop_types ORDER BY crop_type";                               
                        $cropresult = mysqli_query($con, $cropquery);
                
                        echo '<select class="form-control" id="CropType" name="CropType">';
                        while ($row = mysqli_fetch_array($cropresult, MYSQLI_BOTH)) {
                             echo '<option value="'. $row{'crop_type'} . '">' . $row{'crop_type'} . "</option>";
                        }
                        echo '</select>';
                    ?>
                </div>

                <!--page 5-extra-->
                <h2>Other Information</h2>                
                <div class="form-group">
                    <label for="NumberOfTractorOperators">Number of Tractor Operators:*</label>
                    <input type="number" class="form-control" id="NumberofTractorOperators" name="NumberOfTractorOperators" value="1" placeholder="Enter number of tractor operators">
                </div>
                <!--TODO: must add this column to operations table-->
                <div class="form-group">
                    <label for="TractorOperator">Operator Name(s):</label>
                    <input type="text" class="form-control" id="TractorOperator" name="TractorOperator" placeholder="Enter tractor operator's name">
                </div>
                <div class="form-group">
                    <label for="NumberOfWorkers">Number of Workers:*</label>
                    <input type="number" class="form-control" id="NumberOfWorkers" name="NumberOfWorkers" value="0">
                </div>
                <div class="form-group">
                    <label for="NumberOfPasses">Number of Passes:*</label>
                    <input type="number" class="form-control" id="NumberOfPasses" name="NumberOfPasses" value="1">
                </div>
                <div class="form-group">
                    <label for="OperationDescription">Operation Description:</label>
                    <textarea class="form-control" id="OperationDescription" name="OperationDescription" rows="2"></textarea>
                </div>
                <div class="form-group">
                    <label for="OperationComments">Comments:*</label>
                    <textarea class="form-control" id="OperationComments" name="OperationComments" rows="3"></textarea>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Log Operation</button>
            </form>
        </div>
        
        <?php
                //close connection to database
                mysqli_close($con);
        ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
        <!--datepicker built for bootstrap-->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
 
        <script>
            //TODO:truncate operation year work for onchange date
            dateYear(){
            $('#OperationYear').val($('#Date').val());
            }
        </script>
    
       <script>
            $('.form-group.date').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
            });
        </script>
                
    </body>
</html>
