<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Farm Operations Form - Add Tractor</title>

    <!-- Bootstrap core CSS -->
    <link href="includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">

  </head>
<body>
<div class="container">

<?php include ('includes/navigationbar.php'); 
?>

	  <!--page 1-->
      <form role="form" action="public/webapp_add_tractor.php" method="post">
        <h2>Please Enter Information for the New Tractor:</h2>
	    <div class="form-group">
		    <label for="TractorMake">Make:</label>
			<input type="text" class="form-control" id="TractorMake" name="TractorMake">
		</div>
	    <div class="form-group">
		    <label for="TractorBrand">Brand:</label>
			<input type="text" class="form-control" id="TractorBrand" name="TractorBrand">
		</div>
		<div class="form-group">
		    <label for="TractorHorsepower">Horsepower:</label>
			<input type="text" class="form-control" id="TractorHorsepower" name="TractorHorsepower">
		</div>
	    <div class="form-group">
		    <label for="TractorDescription">Description:</label>
			<input type="text" class="form-control" id="TractorDescription" name="TractorDescription" placeholder="Enter Tractor Description">
		</div>
	    <div class="form-group">
		    <label for="TractorDriveType">Drive type:</label>
			<select class="form-control" id="TractorDriveType" name="TractorDriveType">
              <option></option>
			  <option>4WD</option>
			  <option>2WD</option>
			  <option>Track</option>
			  <option>MFWD</option>
              <option>6X4</option>
			</select>
		</div>
		<div class="form-group">
		    <label for="TractorFuelType">Fuel type:</label>
			<select class="form-control" id="TractorFuelType" name="TractorFuelType">
                <option></option>
                <option>Diesel</option>
                <option>Gasoline</option>
            </select>
		</div>
	    <div class="form-group">
		    <label for="TractorComments">Comments:</label>
			<textarea type="text" class="form-control" id="TractorComments" name="TractorComments" rows="3"></textarea>
		</div>


		<button class="btn btn-lg btn-primary btn-block" type="submit">Add Tractor</button>
</form>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
