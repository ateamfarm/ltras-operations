<?php 
    include ('config.php');
    include ('../includes/errormessage.php');
    $con=mysqli_connect($tdbaddr, $tdbuser, $tdbpass, $tdbname, $tdbport);

    $table = "lookup_operation_materials";
    //If variable does not exist then add error to error array
    if (!isset($_POST["material_type"])) {
           $errors['Material Type'] = $errorrequired;
           //Kill page and output message
           die('material type ' . $errors['Material Type']);
    }
    else{
        $material = mysqli_real_escape_string($con, $_POST["material_type"]);

        if(!is_string($material)) {
            die("Error: operation_type must be string.");
        }
        
        // Check the table
        $sql = "SELECT * FROM $table WHERE operations_materials = '$material'";
        $result = mysqli_query($con, $sql);

        if(mysqli_error($con)) {
            die(mysqli_error($con));
        }   

        // Insert if not found
        if(mysqli_num_rows($result) == 0) {
            $sql = "INSERT INTO $table values (DEFAULT, '$material')";
            //mysqli_query($con, $sql);
            if(mysqli_error($con)) {
                die(mysqli_error($con));
            }
        echo "Success";
    }  }
    //Output errors if there are any
    if (!empty($errors)) {
    		echo json_encode($errors);
    }
?>