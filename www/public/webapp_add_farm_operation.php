<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>Operation Summary</title>

        <!-- Bootstrap core CSS -->
        <link href="../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="navbar" rel="stylesheet">
        <link href="../styles/errorsformatting.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <?php
                //TODO: change SQL connection to remote server once the remote server is ready
                include ("../config.php");
                include ("../includes/errormessage.php");
				include ("add_farm_operation.php");
				include_once("../includes/navigationbar.php");

                
                //Only submit fields to DB if all fields check out all right
                if (empty($errors)){
                    //TODO: Make this an include file which only conditionally shows if user
                    //is connected to the webapp
                    
                    //Operation Summary Table
                    echo '<table class="table">';
                    echo "<tr><th>Tractor</th><td>" . $tractorname . "</td></tr>";
                    echo "<tr><th>Equipment</th><td>" . $equipmentdesc . "</td></tr>";
                    echo "<tr><th>Operation Type</th><td>" . $operationtype . "</td></tr>";
                    echo "<tr><th>Date</th><td>" . $inputdate . "</td></tr>";
                    echo "<tr><th>Year</th><td>" . $opyear . "</td></tr>";
                    echo "<tr><th>Material Type</th><td>" . $materialtype . "</td></tr>";
                    echo "<tr><th>Material Description</th><td>" . $materialdesc . "</td></tr>";
                    echo "<tr><th>Material Number/Quantity</th><td>" . $materialquantity . "</td></tr>";
                    echo "<tr><th>Material Units</th><td>" . $materialunits . "</td></tr>";
                    echo "<tr><th>System</th><td>" . $systemname . "</td></tr>";
                    echo "<tr><th>System Code</th><td>" . $systemcode . "</td></tr>";
                    echo "<tr><th>Crop</th><td>" . $croptype . "</td></tr>";
                    echo "<tr><th>Tractor Operators</th><td>" . $numTractorOperators . "</td></tr>";
                    echo "<tr><th>Workers</th><td>" . $numworkers . "</td></tr>";
                    echo "<tr><th>Passes</th><td>" . $numpasses . "</td></tr>";
                    echo "<tr><th>Description</th><td>" . $opdescription . "</td></tr>";
                    echo "<tr><th>Notes</th><td>" . $comments . "</td></tr>";
                    
                    echo "<tr><th>Tractor Operator Name</th><td>" . $tractoroperatorname . "</td></tr>";
                   
                    echo "</table><br/>";
					}
					else{
						foreach($errors as $key => $value) {
							echo("<h3>". $key . ": " . $value . "</h3>");
					}	}
					echo '<div class="form-group">			
                        <p><a href="../farmoperation.php" class="btn btn-default btn-block" role="button">New Operation</a></p>
                    </div>';
            ?>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    </body>
</html>
