<?php 
    include ('../config.php');
    include ('../includes/errormessage.php');
    $con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
    if(mysqli_error($con)) {
        die(mysqli_error($con));
    } 
    
    //If the variable does not exist kill the page
    if (!isset($_POST["crop_type"])) {
       $errors['Crop'] = $errorrequired;
       die('crop ' .$errors['Crop']);
    }
    else{

        $crop = mysqli_real_escape_string($con, $_POST["crop_type"]);
        
        // Check the table for duplicates
        $sql = "select * from lookup_crop_types where crop_type = '$crop';";
        $result = mysqli_query($con, $sql);

        if(mysqli_error($con)) {
            die(mysqli_error($con));
        }

        if(mysqli_num_rows($result) == 0) {
            $sql = "INSERT INTO lookup_crop_types values (DEFAULT, '$crop');";
            //mysqli_query($con, $sql);
            if(mysqli_error($con)) {
                echo mysqli_error($con);
            }
        //echo "Success";
    }   }
    //Output errors if there are any
    if (!empty($errors)) {
    		echo json_encode($errors);
    }
?>