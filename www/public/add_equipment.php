<?php 
	include ('../config.php');
	include ('../includes/errormessage.php');

	//TODO: change SQL connection to remote server once the remote server is ready
	$con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
	// Check connection
    if(mysqli_error($con)) {
        die(mysqli_error($con));
    } 
	// if (mysqli_connect_errno())
	// {
		// $errors['Database Connection'] = "Failed to connect to MySQL: " . mysqli_connect_error();
	// }


	//Values from User
	// $_POST['EquipmentName']
	// $_POST['EquipmentWidth']
	// $_POST['EquipmentDescription']
	// $_POST['EquipmentComments']    

	//Turn post data into PHP variables
	if (!empty($_POST['EquipmentName'])) {
			$equipname = mysqli_real_escape_string($con, $_POST['EquipmentName']);
	}
	else{
		$errors['Equipment Name'] = $errorrequired;
	}
	if (empty($_POST['EquipmentWidth'])){
		$equipwidth = 0;
	}
	else if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['EquipmentWidth'])) {
			$equipwidth = $_POST['EquipmentWidth'];
	}
	else{
		$errors['Width'] = $errordecimals;
	}
	if (!empty($_POST['EquipmentDescription'])) {
			$equipdesc = mysqli_real_escape_string($con, $_POST['EquipmentDescription']);
	}
	else{
		$errors['Equipment Description'] = $errorrequired;
	}
	$equipcomments = mysqli_real_escape_string($con, $_POST['EquipmentComments']);
	/* uncomment if the form ever includes these db values
	if (isset($_POST['ASAENameMatch']))
		$ASAEnamematch = mysqli_real_escape_string($con, $_POST['ASAENameMatch']);
	if (isset($_POST['SpeedLowerLimit'])){
		if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['SpeedLowerLimit'])
			or empty($_POST['SpeedLowerLimit'])) {
				$speedlowerlimit = $_POST['SpeedLowerLimit'];
		}
		else{
			$errors['Speed Lower Limit'] = $errordecimals;
	} }
	if (isset($_POST['SpeedTypical'])) {
		if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['SpeedTypical'])
			or empty($_POST['SpeedTypical'])) {
				$speedtypical = $_POST['SpeedTypical'];
		}
		else{
			$errors['Typical Speed'] = $errordecimals;
	} }
	//Check if typical speed and lower limit have user values
	if (!is_null($speedtypical) && !is_null($speedlowerlimit))
		//if they have user values then make sure typical speed is above 
		//the given speed lower limit
		if ($speedtypical < $speedlowerlimit)
			$errors['Speed Typical'] = $errorlowerlimit;
	if (isset($_POST['EfficiencyLowerLimit'])) {
		if (preg_match("/([0-9]+)/", $_POST['EfficiencyLowerLimit'])
				or empty($_POST['EfficiencyLowerLimit'])) {
			$efficiencylowerlimit = $_POST['EfficiencyLowerLimit'];
		}
		else{
			$errors['Efficiency Lower Limit'] = $errornumbers;
	}  }  
	if (isset($_POST['EfficiencyTypical'])) {
		if (preg_match("/([0-9]+)/", $_POST['EfficiencyTypical'])
				or empty($_POST['EfficiencyTypical'])) {
			$efficiencytypical = $_POST['EfficiencyTypical'];
		}
		else{
			$errors['Efficiency Typical'] = $errornumbers;
	} }
	//Check if typical efficiency and lower limit have user values
	if (!is_null($efficiencytypical) && !is_null($efficiencylowerlimit))
		//if they have user values then make sure typical efficiency is above 
		//the given efficiency lower limit
		if ($efficiencytypical < $efficiencylowerlimit)
			$errors['Typical Efficiency'] = $errorlowerlimit;
			
	//can't seem to find these on the Access table but it shows up in the design view
	if (isset($_POST['EquipmentEstimatedFarmHours']))
		$equipestimatedfarmhrs = $_POST['EquipmentEstimatedFarmHours'];
	if (isset($_POST['EquipmentEstimatedExperimentHours']))
		$equipestimatedexphrs = $_POST['EquipmentEstimatedExperimentHours'];
	if (isset($_POST['EquipmentTheoreticalHours']))
	$equiptheoreticalhrs = $_POST['EquipmentTheoreticalHours'];
	*/

	//If not errors then send it to sql
	if (empty($errors)) {
		//Inserting input form inputs to the DB equipment table
		$sql="INSERT INTO equipment (   equipment_id, 
										equipment_name, 
										width_ft,
										description, 
										`theoretical_hours/acre`, 
										`estimated_farm_hours/acre`,
										`estimated_experiment_hours/acre`, 
										comments, 
										name_match_from_ASAE,
										speed_lower_limit, 
										speed_typical, 
										efficiency_lower_limit,
										efficiency_typical)
			VALUES( DEFAULT, 
					'$equipname', 
					$equipwidth, 
					'$equipdesc', 
					DEFAULT, 
					DEFAULT, 
					DEFAULT, 
					'$equipcomments', 
					DEFAULT, 
					DEFAULT,
					DEFAULT,
					DEFAULT, 
					DEFAULT
			);";

         if(!mysqli_query($con, $sql)) {
                echo mysqli_error($con);
         }
        echo "Success";
	}
	else {
		echo json_encode($errors);
	}
	//close connection to database
	mysqli_close($con);
?>