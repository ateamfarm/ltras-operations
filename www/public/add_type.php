<?php 
    include ('../config.php');
    include ('../includes/errormessage.php');
    $con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
    if(mysqli_error($con)) {
        die(mysqli_error($con));
    } 
    $table = "lookup_operation_types";
    //If variable does not exist then add error to error array
    if (!isset($_POST["operation_type"])) {
           $errors['Operation Type'] = $errorrequired;
           //Kill page and output message
           die('type ' . $errors['Operation Type']);
    }
    else{
    
        $type = mysqli_real_escape_string($con, $_POST["operation_type"]);

        if(!is_string($type)) {
            die("Error: operation_type must be string.");
        }
        
        
        // Check the table
        $sql = "SELECT * FROM $table WHERE operation_type = '$type';";
        $result = mysqli_query($con, $sql);

        if(mysqli_error($con)) {
            die(mysqli_error($con));
        }  

        // Insert if not found
        if(mysqli_num_rows($result) == 0) {
            $sql = "INSERT INTO $table values (DEFAULT, '$type');";
            mysqli_query($con, $sql);
            if(mysqli_error($con)) {
                die (mysqli_error($con));
            }
        //echo "Success";
    }  }
    //Output errors if there are any
    if (!empty($errors)) {
    		echo json_encode($errors);
    }
?>