<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<!-- Feature: consider adding a column to the tractor table for "Retired"
     default boolean to no
     Make a form to change it to retired
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>Add Tractor Form</title>

        <!-- Bootstrap core CSS -->
        <link href="../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="navbar" rel="stylesheet">
        <link href="../styles/errorsformatting.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <?php 
                include ('../includes/navigationbar.php');
				include ('add_tractor.php');
				//If there are errors, display them to the user
				if (!empty($errors)) {
                    foreach($errors as $key => $value) {
                        echo("<h3>". $key . ": " . $value . "</h3>");
                   
				} }
				else {
					echo "<h3> Tractor Added</h3>";
				}
               //Show link to the Add Tractor form
				echo '<div class="form-group">			
                        <p><a href="../find_or_add_tractor.php" class="btn btn-default btn-block" role="button">Add Tractor</a></p>
                     </div>'; 
            ?>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    </body>
</html>
