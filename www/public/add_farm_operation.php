<?php
	//TODO: change SQL connection to remote server once the remote server is ready
	include ("../config.php");
	include ("../includes/errormessage.php");
    
    if (isset($_POST['crop_type'])){
        include ("add_crop.php");
    }
    if (isset($_POST['operation_type'])){
        include ("add_type.php");
    }

	// Hardcoded SQL Connection Example:
	// $con=mysqli_connect("localhost", "root", "", "ltras_full", "3306");
	$con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
    if(mysqli_error($con)) {
        die(mysqli_error($con));
    } 
	
	// Check Connection
	// if (mysqli_connect_errno()) {
		// $errors['Database Connection'] = "Failed to connect to MySQL: " . mysqli_connect_error();
	// }
    
    //boolean to decide whether or not to add to lookup_operation_materials table
    //default to not adding to lookup table
    $addtolookuptable = 0;
	
	// Values from User
	// $_POST['TractorID']
	// $_POST['EquipmentID']
	// $_POST['OperationType']
	// $_POST['Date']
	// $_POST['OperationYear']
	// $_POST['OperationMaterialsID']
	// $_POST['MaterialQuantity']
	// $_POST['MaterialUnits']
	// $_POST['MaterialComments']
	// $_POST['SystemNumber']
	// $_POST['CropType']
	// $_POST['NumberOfTractorOperators']
	// $_POST['NumberOfWorkers']
	// $_POST['NumberOfPasses']
	// $_POST['OperationComments']
	// $_POST['OperationDescription']
	// $_POST['TractorOperator']
	
	//New Version: possible to add materials
	// $_POST['OperationMaterialsMaterial']
	// $_POST['OperationMaterials
	// and change out operationmaterialsID
		 
	if(!(isset($_POST['TractorID']) &&
		 isset($_POST['EquipmentID']) &&
		 isset($_POST['OperationType']) &&
		 isset($_POST['Date']) &&
		 isset($_POST['OperationYear']) &&
		 isset($_POST['MaterialQuantity']) &&
		 isset($_POST['MaterialUnits']) &&
		 isset($_POST['MaterialComments']) &&
		 isset($_POST['SystemNumber']) &&
		 isset($_POST['CropType']) &&
		 isset($_POST['NumberOfTractorOperators']) &&
		 isset($_POST['NumberOfWorkers']) &&
		 isset($_POST['NumberOfPasses']) &&
		 isset($_POST['OperationComments']) &&
		 isset($_POST['OperationDescription']) &&
		 isset($_POST['TractorOperator']))) {
		 
		 $errors['Incomplete Data'] = "Not all data was submitted.";
		 die($errors['Incomplete Data']);
	}
        //set default to id was given
        $nameanddescnotid = 0;
    	if((isset($_POST['MaterialOPName']) &&
             isset($_POST['MaterialOPDescription']))) {
             //Set to true to mean name and desc were given
             $nameanddescnotid = 1;
        }
        else if((isset($_POST['OperationMaterialsID']))){
            //Make sure to set the following variable to false meaning id was given
            $nameanddescnotid = 0;
        }       
        //Otherwise none of the variables exist
        else{
             
             $errors['Incomplete Data'] = "Not all data was submitted.";
             die($errors['Incomplete Data']);
        }

	//TODO: validation of form data
	//escape variables for security
	
	// Turn post data into PHP variables
	//Tractor Values
	if (preg_match("/([0-9]+)/", $_POST['TractorID'])) {
		//The tractor id is the the tractor value of
		//the selected tractor description
		$tractorid = mysqli_real_escape_string($con, $_POST['TractorID']);
		//Pull Tractor Description in order to show in Summary
		$tractorquery =  "SELECT description,tractor_id FROM tractors WHERE tractor_id=$tractorid";                               
		$tractorresult = mysqli_query($con, $tractorquery);
		$tractorrow = mysqli_fetch_row($tractorresult);
		//Tractor Name and TractorDescription are synonymous
		//tractorname will always be the first once since tractor_ids should be unique
		$tractorname = $tractorrow[0];
	}
	else{
		$errors['Tractor ID'] = $errorselectionnotavailable;
	}
	//Equipment Values
	if (preg_match("/([0-9]+)/", $_POST['EquipmentID'])) {
		//The value of the Equipment Description is the equipment id
		$equipid = mysqli_real_escape_string($con, $_POST['EquipmentID']);
		//Pull Equipment Description in order to show in Summary
		$equipquery =  "SELECT equipment_name, description FROM equipment WHERE equipment_id=$equipid";                               
		$equipresult = mysqli_query($con, $equipquery);
		$equiprow = mysqli_fetch_row($equipresult);
		$equipname = $equiprow[0];
		$equipmentdesc = $equiprow[1];
	}
	else {
		$errors['Equipment ID'] = $errorselectionnotavailable;
	}
	//Since operation type can basically be any character,
	//the only validation is through escaping characters
	$operationtype = mysqli_real_escape_string($con, $_POST['OperationType']);
	
	$inputdate = date('Y-m-d H:i:s');
	$opyear = intval(date('Y'));
	if (empty($_POST['Date'])){
		$errors['Date'] = $errorrequired;
	}
	else {
		$inputdate = mysqli_real_escape_string($con, $_POST['Date']);
	}
	if (empty($_POST['OperationYear'])){
		$errors['Operation Year'] = $errorrequired;
	}
	//Otherwise make sure it's a number and one equal or less than the current year
	else if (preg_match("/([0-9]+)/", $_POST['OperationYear']) && $_POST['OperationYear'] <= date('Y')) {
		$opyear = mysqli_real_escape_string($con, $_POST['OperationYear']);
	}
	else {
		$errors['Operation Year'] = $erroryear;
	}

    //Assume material id was given
	if(0 == $nameanddescnotid){
        //Validate Material Type (value is operation_materials_id)
        if (preg_match("/([0-9]+)/", $_POST['OperationMaterialsID'])) {
            //The operation_materials_id is the the material value of
            //the selected material
            $opmaterialid = mysqli_real_escape_string($con, $_POST['OperationMaterialsID']);
            //Pull material and description to show in Summary
            $opmatquery =  "SELECT material,description FROM operation_materials WHERE operation_materials_id=$opmaterialid";                               
            $opmatresult = mysqli_query($con, $opmatquery);
            $opmatrow = mysqli_fetch_row($opmatresult);
            //material name/type will always be the first once since operation_materials_ids should be unique
            //In this file only, material name is also material type
            $materialtype = $opmatrow[0];
            $materialdesc = $opmatrow[1];
        }
        else{
            $errors['Material Type'] = $errorselectionnotavailable;
        }
    }
    //Assume material name and type were given
    else if (1 == $nameanddescnotid){
        //Then validate name and description
        if (!empty($_POST['MaterialOPName'])) {
                $matopname = mysqli_real_escape_string($con, $_POST['MaterialOPName']);
                //Check if the material already exists in the lookup table
                $lookupquery =  "SELECT * FROM lookup_operation_materials WHERE operations_materials='$matopname'";   
                $lookupresult = mysqli_query($con, $lookupquery);
                if (isset($lookupresult) and !is_null($lookupresult)){
                    $lookuprow = mysqli_fetch_row($lookupresult);
                    //If there isn't something in here then the material must not already exist in the lookup table
                    if(is_null($lookuprow[0])) {
                        //Therefore set add to lookup table to true;
                        $addtolookuptable = 1;
                    }
                }
        }
        else{
            $errors['Material Name'] = $errorrequired;
        }
        //Material Description can be any character
        if (!empty($_POST['MaterialOPDescription'])) {
                $matopdesc = mysqli_real_escape_string($con, $_POST['MaterialOPDescription']);
                //make sure material name exists
                if(isset($matopname)){
                    //Check to make sure the material does not already exist in the database
                    $matopquery =  "SELECT operation_materials_id FROM operation_materials WHERE description='$matopdesc' AND material='$matopname'";   
                    $matopresult = mysqli_query($con, $matopquery);
                    if (isset($matopresult) and !is_null($matopresult)){
                        $matoprow = mysqli_fetch_row($matopresult);
                        //If there is something in here then the description must already exist in the database
                        if(!is_null($matoprow[0])) {
                            //assign opmaterialid to the first operation material fetched
                            $opmaterialid = $matoprow[0];
                        }
                    }
                }
        }
        else{
            $errors['Material Description'] = $errorrequired;
        }
    }
    
	//Make sure Material Quantity is only an integer or decimal
	if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['MaterialQuantity']))	{
			$materialquantity = mysqli_real_escape_string($con, $_POST['MaterialQuantity']);
	}
	else{
		$errors['Quantity'] = $errordecimals;
	}
	//Make sure the material units field exists in the form
	if (isset($_POST['MaterialUnits']))
		$materialunits = mysqli_real_escape_string($con, $_POST['MaterialUnits']);
	//Comments field does not exist in this version but 
	//if future versions ask for it,
	//Make sure the comments field exists in the form and prevent SQL injections
	if (isset($_POST['MaterialComments']))
		$matcomments = mysqli_real_escape_string($con, $_POST['MaterialComments']);
	else
		$matcomments = "";
	
	//Validate System Code as only capital letters and numbers
	//TODO: validate this pattern number-numberLetter or no letter
	if (preg_match("/([0-9]+)/", $_POST['SystemNumber'])) {
		//The systemnumber is the the system value of
		//the selected system
		$systemnumber = mysqli_real_escape_string($con, $_POST['SystemNumber']);
		//Retrieve the name and code of the system from the database
		$systemquery =  "SELECT system_name,system_code FROM system_names WHERE system_number=$systemnumber";                               
		$systemresult = mysqli_query($con, $systemquery);
		$systemrow = mysqli_fetch_row($systemresult);
		//Values are straight from DB so they shouldn't need validation
		$systemname = $systemrow[0];
		$systemcode = $systemrow[1];
	}
		
	//Validate Crop Type as only letters
	if (preg_match("/([A-Za-z]+)/", $_POST['CropType']))
		$croptype = mysqli_real_escape_string($con, $_POST['CropType']);
	else
		$errors['Crop Type'] = $errorletters;


	// Error Checking/Form Validation
	//Tractor Operators: Validate that they're whole numbers
	if (preg_match("/([0-9]+)/", $_POST['NumberOfTractorOperators'])) {
		$numTractorOperators = mysqli_real_escape_string($con, $_POST['NumberOfTractorOperators']);
		//If there is at least one tractor operator and its name is filled out then submit it to database
		if (($numTractorOperators > 0) and (!empty($_POST['TractorOperator'])))
			//Protect Operator Names from SQL injections
			$tractoroperatorname = mysqli_real_escape_string($con, $_POST['TractorOperator']);
		else
			$errors['Tractor Operator Name'] = 'error';
	}
	else{
		$errors['Number of Tractor Operators'] = $errornumbers;
	}
	//Workers: Validate only integers
	if(preg_match("/([0-9]+)/", $_POST['NumberOfWorkers'])) {
		$numworkers = mysqli_real_escape_string($con, $_POST['NumberOfWorkers']);
	} else {
		$errors['Workers'] = $errornumbers;
	}
	//Passes: Validate only ints
	if(preg_match("/([0-9]+)/", $_POST['NumberOfPasses'])) {
		$numpasses = mysqli_real_escape_string($con, $_POST['NumberOfPasses']);
	} else {
		$errors['Passes'] = $errornumbers;
	}
	//Operation Description: only SQL injections are not allowed
	$opdescription = mysqli_real_escape_string($con, $_POST['OperationDescription']);
	//Operation Comments: only SQL injections are not allowed
	$comments = mysqli_real_escape_string($con, $_POST['OperationComments']);
	//Dataset 547 means Israel enters the farm operations data into the database himself
	//But we're using zero because that's what the Acces database had
	$datasetid = 0;
	
	//Fill in Operation Details Table					
	//OperationDetailsID is auto-incremented and plot name is derived from
	//looking up plots from system name in plot_characteristics
	//then the operation_details can be filled in
	//Retrieve the plot from the given system number	
	$operationdetailquery =  "SELECT plot FROM plot_characteristics WHERE system_number='$systemnumber'";                               
	$operationdetailresult = mysqli_query($con, $operationdetailquery);
	//Throw an error if the system number does not equal 101 (aka unassigned) 
	//  and the query does not retrieve any plots
	if (!isset($operationdetailresult) and $systemnumber != '101'){
		$errors['System Plots'] = "All systems, except for Unassigned must have plots";}
	
	//Only submit fields to DB if all necessary insert fields check out all right
	if (empty($errors)){
        
        //If material name and description were given then need to add to operations_materials table to have an id
        //Unless the operation material id has already been assigned
        if ($nameanddescnotid and !isset($opmaterialid)){
            //Inserting input form inputs and default values to the DB equipment table
            $opmatsql="INSERT INTO operation_materials ( operation_materials_id, 
                                                    material, 
                                                    description, 
                                                    `nitrogen_content_%`, 
                                                    `phosphorus_content_%`,
                                                    `potassium_content_%`,
                                                    `sulfur_content_%`,
                                                    ratio_phosphorus,
                                                    ratio_potassium,
                                                    ratio_sulphur,
                                                    comments)
                VALUES( DEFAULT, 
                        '$matopname', 
                        '$matopdesc', 
                        0, 
                        0, 
                        0, 
                        0, 
                        0, 
                        0, 
                        0,
                        ' '
                );";

            // Check for Errors in SQL query
            $opmatresult = mysqli_query($con,$opmatsql);
            if (false === $opmatresult) {
                die('Error: ' . mysqli_error($con) . "<br/>");
            } else {
                //This line assigns the last ID inserted to operation materials
                $opmaterialid = mysqli_insert_id($con);
                //echo "operation added<br/>";
            }
            
            //Add material to lookup table if variable is set to true;
            if ($addtolookuptable) {
                //Inserting materials inputs to the lookup table
                $lookuptablesql = "INSERT INTO lookup_operation_materials values (DEFAULT, '$matopname')";
                if (!mysqli_query($con,$lookuptablesql)) {
                    die('Error: ' . mysqli_error($con) . '<br/>');
        }   }   }
    
		$sql="INSERT INTO operations (operation_id,
									  dataset_id,
									  date,
									  operation_year,
									  operation_type,
									  equipment_id,
									  tractor_id,
									  description,
									  number_of_passes,
									  number_of_tractor_operators,
									  number_of_workers,
									  comments,
									  tractor_operator_name)
			 VALUES (DEFAULT,
					 $datasetid,
					 '$inputdate',
					 $opyear,
					 '$operationtype',
					 $equipid,
					 $tractorid,
					 '$opdescription',
					 $numpasses,
					 $numTractorOperators,
					 $numworkers,
					 '$comments',
					 '$tractoroperatorname');";

		//Check if the sql is fully formatted
		//echo $sql;
		
		//echo "<br/>";
	
		// Check for Errors in SQL query
		$result = mysqli_query($con,$sql);
		if (false === $result) {
			die('Error: ' . mysqli_error($con) . "<br/>");
		} else {
			//This line assigns the last ID inserted to operationid
			$operationid = mysqli_insert_id($con);
			//echo "operation added<br/>";
		}
		
		//Inserting materials inputs to the DB material table
		$materialsql="INSERT INTO materials (   material_id, 
										operations_id,
										operation_materials_id,
										material_quantity,
										material_units,
										comments)
						VALUES( DEFAULT, 
								$operationid, 
								$opmaterialid, 
								$materialquantity, 
								'$materialunits', 
								'$matcomments'
			);";

		if (!mysqli_query($con,$materialsql)) {
			die('Error: ' . mysqli_error($con) . '<br/>');
		}
		//else
		//    echo "materials added<br/>";
		
		//Store Crop and System info in the System and crop table
		$systemcroptablesql="INSERT INTO system_crops ( system_crop_id,
														system_number,
														crop_type,
														operation_id)
						VALUES( DEFAULT, 
								$systemnumber, 
								'$croptype', 
								$operationid
			);";

		if (!mysqli_query($con,$systemcroptablesql)) {
			die('Error: ' . mysqli_error($con) . '<br/>');
		}
		//Otherwise details were added to system_crops table
		//else
		//    echo "system crops added<br/>";
		
		//Fill in Operation Details Table					
		//Keep saving plots until no more plots exist
		while ($operationdetailrow = mysqli_fetch_array($operationdetailresult, MYSQLI_BOTH)) {				
			$plotname = $operationdetailrow{'plot'};
			$operationdetailsql = "INSERT INTO operation_details ( 	operation_detail_id,
																	operation_id,
																	plot)
									VALUES (DEFAULT,
											$operationid,
											'$plotname')";
					 
			if (!mysqli_query($con,$operationdetailsql)) {
				die('Error: ' . mysqli_error($con) . '<br/>');
			}
			//Otherwise operation detail was added
			/*else
				echo "operation detail added";
			*/
		}
		echo "Success";
	}
	//Otherwise show user the validation errors
	else {
		echo json_encode($errors);
	}
	
	// Close connection to database
	mysqli_close($con);
?>