<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Equipment Summary</title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">
    <link href="../styles/errorsformatting.css" rel="stylesheet">

  </head>
<body>
<div class="container">
    <?php 
	include ('../includes/navigationbar.php');
    include ('../includes/errormessage.php');
	include ('add_equipment.php');

    //TODO: output the input as a summary for the user
    if (empty($errors)) {
        echo '<h2>Equipment Summary</h2>';
        echo '<table class="table">';
        echo '<tr><th>Name</th><td>' . $equipname . " </td></tr>
              <tr><th>Width</th><td>" . $equipwidth . " ft </td></tr>";
        echo "<tr><th>Description</th><td> " . $equipdesc . "</td></tr>"; 
        echo "<tr><th>Comments</th><td> " . $equipcomments . "</td></tr>";
		/*
        echo "<tr><th>Name Match from ASAE</th><td>" . $ASAEnamematch . "</td></tr>";
        echo "<tr><th>Speed (lower limit)</th><td>" . $speedlowerlimit . " mph</td></tr>";
        echo "<tr><th>Speed (typical)</th><td>" . $speedtypical . " mph</td></tr>";
        echo "<tr><th>Efficiency (lower limit)</th><td>" . $efficiencylowerlimit . "%</td></tr>";
        echo "<tr><th>Efficiency (typical)</th><td>" . $efficiencytypical . "%</td></tr>";
		//*/
        echo "</table><br/>";
	}
    else {
        foreach($errors as $key => $value) {
            echo("<h3>". $key . ": " . $value . "</h3>");
        }
    }
    echo '<div class="form-group">			
                <p><a href="../FindAddEquipment.php" class="btn btn-default btn-block" role="button">Add Equipment</a></p>
            </div>';
    ?>
</div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
