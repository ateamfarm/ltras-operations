<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>A-Team LTRAS Project</title>

    <!-- Bootstrap -->
    <link href="includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">

  </head>
<body>
<div class="container">

     <?php
        $_SESSION['ticket'] = isset($_GET['ticket']) ? $_GET['ticket'] : null;
        if (isset($_SESSION['ticket'])) {
            include('includes/navigationbar.php');
        }
        else {   
            echo '
              <div class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand active" href="index.php">LTRAS</a>
                  </div>
                  <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                      <li><a href="https://cas.ucdavis.edu/cas/login?service=http://rrproject2.lawr.ucdavis.edu/index.php">Sign in</a></li>
                    </ul>
                  </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
              </div>';
          }
    ?>
    
    <div class="jumbotron">
        <h1>LTRAS Farm Operations Portal</h1>
        <p class="lead">Add farming operations via the web.</p>
		<p>Made by Judy Fong, Andrew McAllister, Cameron Massoudi, and Michael Nowak</p>
        <p><a class="btn btn-lg btn-success" href="https://cas.ucdavis.edu/cas/login?service=http://rrproject2.lawr.ucdavis.edu/index.php" role="button">Sign in</a></p>
    </div>

		
  </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  </body>
</html>