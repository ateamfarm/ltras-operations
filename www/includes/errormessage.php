<?php
$errornumbers = "only numbers accepted";
$errordecimals = "only numbers or decimal numbers accepted";
$errornumbersandletters = "only numbers and letters accepted";
$errorletters = "only letters accepted";
$errorrequired = "is a required field";
$errorlowerlimit = "must be equal or above the lower limit";
$errorselectionnotavailable = "selection not found";
$errorcapitalsandnumbers = "only capital letters and numbers accepted";
$erroryear = "must be a four digit current or prior year";
$errordate = "must be a prior or current date";
?>