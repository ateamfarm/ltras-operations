<?php
    define('WEBROOT', getBaseURL());

    //a way to get the base url
    function getBaseURL() {
        return 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://'
             . $_SERVER['HTTP_HOST']
             . '/'
        ;
    }
?>
<div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <?php 
                echo '<a class="navbar-brand" href="' . WEBROOT . 'index.php">LTRAS</a>';
            ?>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li>
                  <?php 
                        echo '<a href="https://cas.ucdavis.edu/cas/logout?service=' . WEBROOT . 'index.php&ticket='. $_SESSION['ticket'] . '">Logout</a>';
                  ?>
              </li>
			  <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
                <ul class="dropdown-menu">
				  <li><?php 
                        echo '<a href="' . WEBROOT . 'admin/FindAddMaterial.php">Add New Material</a>';
                        ?></li>
                  <!--No new systems are allowed to be added via the web
                    <li><?php echo '<a href="' . WEBROOT . 'admin/FindAddSystem.php">Add New System</a>'; ?></li>
                    -->
                  <li><?php echo '<a href="' . WEBROOT . 'admin/FindAddCrop.php">Add New Crop</a>'; ?></li>
                </ul>
              </li>
			  <li><?php echo '<a href="' . WEBROOT . 'farmoperation.php">Single Page Form</a>'; ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>