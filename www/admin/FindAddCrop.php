<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Add Crop</title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">

  </head>
    <body>
        <div class="container">
            <?php include ('../includes/navigationbar.php'); ?>
            <form role="form" action="process_admin_forms/add_crop.php" method="post">
                <h2>Find or Add Crop</h2>
                <div class="form-group">
                    <label for="CropType">Crop:</label>
                    <input type="text" class="form-control" id="CropType" name="CropType">
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Add Crop</button>
            </form>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    </body>
</html>
