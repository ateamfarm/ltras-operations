<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Crop Summary</title>

    <!-- Bootstrap core CSS -->
    <link href="../../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">
    <link href="../../styles/errorsformatting.css" rel="stylesheet">

  </head>
<body>
<div class="container">

    <?php 
    
    include ('../../config.php');
    include ('../../includes/errormessage.php');

    //TODO: change SQL connection to remote server once the remote server is ready
    $con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
    // Check connection
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    //Turn post data into PHP variables
	//Validate Crop Type as only letters
	if (isset($_POST['CropType']) and preg_match("/([A-Za-z]+)/", $_POST['CropType'])) {
		$croptype = mysqli_real_escape_string($con,$_POST['CropType']);
		//Check to make sure the crop does not already exist in the database
		$cropquery =  "SELECT crop_type FROM lookup_crop_types WHERE crop_type='$croptype'";   
		$cropresult = mysqli_query($con, $cropquery);
		if (isset($cropresult) and !is_null($cropresult)){
            $croprow = mysqli_fetch_row($cropresult);
            //If there is something in here then the croptype must already exist in the database
            if(!is_null($croprow[0])) {
                $errors['Crop'] = "duplicate crop";
            }
		}
	}
	else
		$errors['Crop'] = $errorletters;
		
    //TODO: output the input as a summary for the user
    if (empty($errors)) {
        echo "<h2>Crop Summary</h2>";
        echo '<table class="table">';
        echo '<tr><th>Crop Type</th><td>' . $croptype . " </td></tr>";
        echo "</table><br/>";

        //Inserting input form inputs to the DB system_names table
        $sql="INSERT INTO lookup_crop_types (crop_type)
            VALUES( '$croptype');";

        if (!mysqli_query($con,$sql)) {
            die('Error: ' . mysqli_error($con) . '<br/>');
        }
        echo "Crop added";
    }
    else {
        foreach($errors as $key => $value) {
            echo("<h3>". $key . ": " . $value . "</h3>");
        }
    }
    echo '<div class="form-group">			
                <p><a href="../FindAddCrop.php" class="btn btn-default btn-block" role="button">Add Crop</a></p>
            </div>';

    //close connection to database
    mysqli_close($con);
    ?>
</div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
