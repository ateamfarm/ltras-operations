<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Material Summary</title>

    <!-- Bootstrap core CSS -->
    <link href="../../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">
    <link href="../../styles/errorsformatting.css" rel="stylesheet">

  </head>
<body>
<div class="container">

    <?php 
    
    include ('../../config.php');
    include ('../../includes/navigationbar.php');
    include ('../../includes/errormessage.php');

    //TODO: change SQL connection to remote server once the remote server is ready
    $con=mysqli_connect($dbaddr, $dbuser, $dbpass, $dbname, $dbport);
    // Check connection
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    //boolean to decide whether or not to add to lookup table
    //default to not adding to lookup table
    $addtolookuptable = 0;

    //TODO: form data validation
    //escape variables for security
    //Turn post data into PHP variables
	//Material Name can be any character since they will all be escaped
    
    //Make sure the post Material Name and Description exist
    if(!(isset($_POST['MaterialOPName']) &&
		 isset($_POST['MaterialOPDescription']))) {
		 
		 $errors['Incomplete Data'] = "Not all data was submitted.";
	}
    //Otherwise validate the name and description values
    else {
        if (!empty($_POST['MaterialOPName'])) {
                $matopname = mysqli_real_escape_string($con, $_POST['MaterialOPName']);
                //Check if the material already exists in the lookup table
                $lookupquery =  "SELECT * FROM lookup_operation_materials WHERE operations_materials='$matopname'";   
                $lookupresult = mysqli_query($con, $lookupquery);
                if (isset($lookupresult) and !is_null($lookupresult)){
                    $lookuprow = mysqli_fetch_row($lookupresult);
                    //If there isn't something in here then the material must not already exist in the lookup table
                    if(is_null($lookuprow[0])) {
                        //Therefore set add to lookup table to true;
                        $addtolookuptable = 1;
                    }
                }
        }
        else{
            $errors['Material Name'] = $errorrequired;
        }
        //Material Description can be any character
        if (!empty($_POST['MaterialOPDescription'])) {
                $matopdesc = mysqli_real_escape_string($con, $_POST['MaterialOPDescription']);
                //make sure material name exists
                if(isset($matopname)){
                    //Check to make sure the material does not already exist in the database
                    $matopquery =  "SELECT description FROM operation_materials WHERE description='$matopdesc' AND material='$matopname'";   
                    $matopresult = mysqli_query($con, $matopquery);
                    if (isset($matopresult) and !is_null($matopresult)){
                        $matoprow = mysqli_fetch_row($matopresult);
                        //If there is something in here then the description must already exist in the database
                        if(!is_null($matoprow[0])) {
                            $errors['Material Description'] = "duplicate description";
                        }
                    }
                }
        }
        else{
            $errors['Description'] = $errorrequired;
        }
        //Comments don't aren't required so only prevent SQL injections
        if (isset($_POST['MaterialOPComments']))
            $matopcomments = mysqli_real_escape_string($con, $_POST['MaterialOPComments']);
    }
      
    //can't seem to find these on the Access form but shows up on table
    //TODO: Ask Emma about these rows
	
	//Default values
	$nitrogencontent = $phosphoruscontent = $potassiumcontent = 0;
	$sulfurcontent = $ratiophosphorus = $ratiopotassium = 0;
	$ratiosulfur = 0;
	//Optional fields use isset to make sure the form field still exists
	//Verify all four Content fields are only decimal or integer
	if (isset($_POST['NitrogenContentPercentage'])) {
		if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['NitrogenContentPercentage'])
			or empty($_POST['NitrogenContentPercentage'])) {
				$nitrogencontent = $_POST['NitrogenContentPercentage'];
		}
		else{
			$errors['Nitrogen Content'] = $errordecimals;
    }	}
	if (isset($_POST['PhosphorusContentPercentage'])){
	if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['PhosphorusContentPercentage'])
        or empty($_POST['PhosphorusContentPercentage'])) {
            $phosphoruscontent = $_POST['PhosphorusContentPercentage'];
    }
    else{
        $errors['Phosphorus Content'] = $errordecimals;
    } }
	if (isset($_POST['PotassiumContentPercentage'])){
		if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['PotassiumContentPercentage'])
			or empty($_POST['PotassiumContentPercentage'])) {
				$potassiumcontent = $_POST['PotassiumContentPercentage'];
		}
		else{
			$errors['Potassium Content'] = $errordecimals;
    }	}
	if (isset($_POST['SulfurContentPercentage'])){
		if (preg_match("/^[1-9]\d*(\.\d+)?$/", $_POST['SulfurContentPercentage'])
			or empty($_POST['SulfurContentPercentage'])) {
				$sulfurcontent = $_POST['SulfurContentPercentage'];
		}
		else{
			$errors['Sulfur Content'] = $errordecimals;
    }}
	//Verify all ratios are whole numbers
	if (isset($_POST['PhosphorusRatio'])){
		if (preg_match("/([0-9]+)/", $_POST['PhosphorusRatio'])
				or empty($_POST['PhosphorusRatio'])) {
			$ratiophosphorus = $_POST['PhosphorusRatio'];
		}
		else{
			$errors['Phosphorus Ratio'] = $errornumbers;
    }	}
	if (isset($_POST['Potassium'])){
		if (preg_match("/([0-9]+)/", $_POST['PotassiumRatio'])
				or empty($_POST['PotassiumRatio'])) {
			$ratiopotassium = $_POST['PotassiumRatio'];
		}
		else{
			$errors['Potassium Ratio'] = $errornumbers;
    }}
	if (isset($_POST['SulfurRatio'])){
		if (preg_match("/([0-9]+)/", $_POST['SulfurRatio'])
				or empty($_POST['SulfurRatio'])) {
			$ratiosulfur = $_POST['SulfurRatio'];
		}
		else{
			$errors['Sulfur	Ratio'] = $errornumbers;
    } }

    //Output the input as a summary for the user
    if (empty($errors)) {
        echo "<h2>Material Summary</h2>";
        echo '<table class="table">';
        echo '<tr><th>Name</th><td>' . $matopname . " </td></tr>";
        echo "<tr><th>Description</th><td> " . $matopdesc . "</td></tr>"; 
        echo "<tr><th>Comments</th><td> " . $matopcomments . "</td></tr>";
		/* Commented out until the form fields are needed again
        echo "<tr><th>Nitrogen Content</th><td>" . $nitrogencontent . "%</td></tr>";
        echo "<tr><th>Phosphorus Content</th><td>" . $phosphoruscontent . "%</td></tr>";
        echo "<tr><th>Potassium Content</th><td>" . $potassiumcontent . "%</td></tr>";
        echo "<tr><th>Sulfur Content</th><td>" . $sulfurcontent . "%</td></tr>";
        echo "<tr><th>Ratio (phosphorus/nitrogen)</th><td>" . $ratiophosphorus . "</td></tr>";
        echo "<tr><th>Ratio (potassium/nitrogen)</th><td>" . $ratiopotassium . "</td></tr>";
        echo "<tr><th>Ratio (sulfur/nitrogen)</th><td>" . $ratiosulfur . "</td></tr>";       
		//*/		
        echo "</table><br/>";

        
        //Inserting input form inputs to the DB equipment table
        $sql="INSERT INTO operation_materials ( operation_materials_id, 
                                                material, 
                                                description, 
                                                `nitrogen_content_%`, 
                                                `phosphorus_content_%`,
                                                `potassium_content_%`,
                                                `sulfur_content_%`,
                                                ratio_phosphorus,
                                                ratio_potassium,
                                                ratio_sulphur,
                                                comments)
            VALUES( DEFAULT, 
                    '$matopname', 
                    '$matopdesc', 
                    '$nitrogencontent', 
                    '$phosphoruscontent', 
                    '$potassiumcontent', 
                    '$sulfurcontent', 
                    '$ratiophosphorus', 
                    '$ratiopotassium', 
                    '$ratiosulfur',
                    '$matopcomments'
            );";

        if (!mysqli_query($con,$sql)) {
            die('Error: ' . mysqli_error($con) . '<br/>');
        }
        echo "Material added";
        
        //Add material to lookup table if variable is set to true;
        if ($addtolookuptable) {
            //Inserting materials inputs to the lookup table
            $lookuptablesql = "INSERT INTO lookup_operation_materials values (DEFAULT, '$matopname')";
            if (!mysqli_query($con,$lookuptablesql)) {
                die('Error: ' . mysqli_error($con) . '<br/>');
        }   }
    }
	//Print the errors for the users
    else {
        foreach($errors as $key => $value) {
            echo("<h3>". $key . ": " . $value . "</h3>");
        }
    }
	//Display link for user to Add another Material
    echo '<div class="form-group">			
                <p><a href="../FindAddMaterial.php" class="btn btn-default btn-block" role="button">Add Material</a></p>
            </div>';

    //close connection to database
    mysqli_close($con);
    ?>
</div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
