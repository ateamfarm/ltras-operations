<?php
	session_start();
	//If the user is not logged in, kill page
	if(!isset($_SESSION['ticket'])) {
		//But before killing the page, redirect them to sign in
		header('Location: ../index.php');
		die('Access to this page only allowed to logged in users. 
			<p><a href="../index.php" class="btn btn-default btn-block" role="button">Sign in</a></p>');
	} 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Add Material</title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar" rel="stylesheet">

  </head>
    <body>
        <div class="container">
            <?php include ('../includes/navigationbar.php'); ?>
            <form role="form" action="process_admin_forms/add_material.php" method="post">
                <h2>Find or Add Material</h2>
                <div class="form-group">
                    <label for="MaterialOPName">Name:</label>
                    <input type="text" class="form-control" id="MaterialOPName" name="MaterialOPName" placeholder="Enter Material Name">
                </div>
                <div class="form-group">
                    <label for="MaterialOPDescription">Description:</label>
                    <textarea type="text" class="form-control" id="MaterialOPDescription" name="MaterialOPDescription" rows="2"></textarea>
                </div>
                <div class="form-group">
                    <label for="MaterialOPComments">Comments:</label>
                    <input type="text" class="form-control" id="MaterialOPComments" name="MaterialOPComments">
                </div>
				<!--these exist in case emma needs them still--
				<h2>Details on Material Table (necessary?)</h2>
                <div class="form-group">
                    <label for="NitrogenContentPercentage">Nitrogen Content Percentage:</label>
                    <input type="number" class="form-control" id="NitrogenContentPercentage" name="NitrogenContentPercentage" step="any">
                </div>
                <div class="form-group">
                    <label for="PhosphorusContentPercentage">Phosphorus Content Percentage:</label>
                    <input type="number" class="form-control" id="PhosphorusContentPercentage" name="PhosphorusContentPercentage" step="any">
                </div>
                <div class="form-group">
                    <label for="PotassiumContentPercentage">Potassium Content Percentage:</label>
                    <input type="number" class="form-control" id="PotassiumContentPercentage" name="PotassiumContentPercentage" step="any">
                </div>
				<div class="form-group">
                    <label for="SulfurContentPercentage">Sulfur Content Percentage:</label>
                    <input type="number" class="form-control" id="SulfurContentPercentage" name="SulfurContentPercentage" step="any">
                </div>
                <div class="form-group">
                    <label for="PhosphorusRatio">Phosphorus/Nitrogen Ratio:</label>
                    <input type="number" class="form-control" id="PhosphorusRatio" name="PhosphorusRatio">
                </div>
                <div class="form-group">
                    <label for="PotassiumRatio">Potassium/Nitrogen Ratio:</label>
                    <input type="number" class="form-control" id="PotassiumRatio" name="PotassiumRatio">
                </div>
				<div class="form-group">
					<label for="SulfurRatio">Sulfur/Nitrogen Ratio:</label>
					<input type="number" class="form-control" id="SulfurRatio" name="SulfurRatio">
			    </div>
				-->
                <button class="btn btn-lg btn-primary btn-block" type="submit">Add Material</button>
            </form>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../includes/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    </body>
</html>
