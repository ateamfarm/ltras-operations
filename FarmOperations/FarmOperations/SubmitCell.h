//
//  SubmitCell.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/29/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *Date;
@property (strong, nonatomic) IBOutlet UILabel *System;
@property (strong, nonatomic) IBOutlet UILabel *Tractor;

@end
