//
//  ViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/14/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "ViewController.h"
#import "AutocompletionTableView.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"
#import "GlobalVariables.h"

@interface ViewController ()

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UILabel *sampleLabel;

@end

@implementation ViewController

@synthesize textField = _textField;
@synthesize sampleLabel = _sampleLabel;
@synthesize autoCompleter = _autoCompleter;

//Autocomplete function
- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.textField inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        
        
        NSArray *operationTractorInfo = [[OperationsDatabase database] getAllTractors];
        NSMutableArray *tractorData;
        for(OperationsInfo *info in operationTractorInfo)
        {
            NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsEquipment, nil];
            tractorData = [NSMutableArray arrayWithArray:tractorData];
            [tractorData addObjectsFromArray:temp];
        }
        
        NSArray *temp = [tractorData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        _autoCompleter.suggestionsDictionary = temp;
    }
    return _autoCompleter;
}

- (void)viewDidLoad
{
    //Load saved global variables into optionsSingle variable.
    optionsSingle = [GlobalVariables singleObj];
    //Set the text field to equal the global variable if it is already set.
    _Tractor.text = optionsSingle.tractorFinal;
    
    [super viewDidLoad];
    
    // set "value changed" event handler for TextField
    [self.textField addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    //Set the tractor's delegate method to itself
    self.Tractor.delegate = self;
}

//Keyboard manipulation. Function allows the "go" button to send you
//to the next view.
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if([self shouldPerformSegueWithIdentifier:@"tractorToEquipment" sender:self])
    {
        [self performSegueWithIdentifier:@"tractorToEquipment" sender:self];
    }
    return NO;
}

- (void)viewDidUnload {
    [self setSampleLabel:nil];
    [self setSampleLabel:nil];
    [super viewDidUnload];
}

#pragma mark - AutoCompleteTableViewDelegate

//Autocomplete function
- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    
    NSArray *operationTractorInfo = [[OperationsDatabase database] getAllTractors];
    NSMutableArray *tractorData;
    for(OperationsInfo *info in operationTractorInfo)
    {
        //NSLog(@"%@", info.operationsTractor);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsTractor, nil];
        tractorData = [NSMutableArray arrayWithArray:tractorData];
        [tractorData addObjectsFromArray:temp];
    }
    
    NSArray *temp = [tractorData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return temp;
}

//Autocomplete function
- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    //NSLog(@"%@ - Suggestion chosen: %d", completer, index);
}

//Error checking method that pops up alert if incorrect data is inputted
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    //Sets global variable holding tractor data equal to the text field.
    optionsSingle.tractorFinal = self.Tractor.text;
    //Check to see if the tractor is in the database.
    int error = [[OperationsDatabase database] getTractorID];
    
    //Check segue
    if([identifier isEqualToString:@"tractorToEquipment"])
    {
        if(error < 0)
        {
            //Pop up alert about the tractor not being in the database
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Error" message:@"Tractor not found in database" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:@"Add Tractor", nil];
            [alert show];
            return NO;
        }
    }
    //Check segue
    if([identifier isEqualToString:@"updateDB"])
    {
        //If not connected to internet, put up error.
        if(![self connected])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Error" message:@"You do not have an internet connection. Please wait until you have a connection to update your database." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

//Defines what happens if you want to add a new tractor
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self performSegueWithIdentifier:@"AddTractor" sender:self];
    }
}

//Operations that occur before the app segues
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier]  isEqualToString:@"tractorToEquipment"] || [[segue identifier]  isEqualToString:@"AddTractor"])
    {
        //Sets global variable holding tractor data equal to the text field.
        optionsSingle.tractorFinal = self.Tractor.text;
    }
}

//Check if connected to internet.
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


@end
