//
//  SysViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "SysViewController.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface SysViewController ()

@property (strong, nonatomic) NSArray *array;
@property (strong, nonatomic) NSArray *cropArray;

@end

@implementation SysViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    
    NSLog(@"System Final = %@", optionsSingle.systemFinal);
    
    /*add query in opdatabase that finds the row in the database based on passing
     in a string*/
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"tractor=%@, ", optionsSingle.tractorFinal);
    NSArray *operationSystems = [[OperationsDatabase database] getAllSystems];
    NSMutableArray *systemsData;
    for(OperationsInfo *info in operationSystems)
    {
        //NSLog(@"%@", info.operationsSystem);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsSystem, nil];
        systemsData = [NSMutableArray arrayWithArray:systemsData];
        [systemsData addObjectsFromArray:temp];
    }
    
    NSArray *operationCrops = [[OperationsDatabase database] getAllCrops];
    NSMutableArray *cropsData;
    for(OperationsInfo *info in operationCrops)
    {
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsCrop, nil];
        cropsData = [NSMutableArray arrayWithArray:cropsData];
        [cropsData addObjectsFromArray:temp];
    }

    NSArray *temp = [systemsData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *temp2 = [cropsData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableArray *systemData = [[NSMutableArray alloc] init];
    [systemData addObject:@"Unassigned"];
    for(int i = 0; i < [temp count]; i++)
    {
        if(![[temp objectAtIndex:i] isEqualToString:@"Unassigned"])
        {
            [systemData addObject:[temp objectAtIndex:i]];
        }
    }
    
    NSMutableArray *cropData = [[NSMutableArray alloc] init];
    [cropData addObject:@"New Crop"];
    for(int i = 0; i < [temp2 count]; i++)
    {
        if(![[temp objectAtIndex:i] isEqualToString:@"New Crop"])
        {
            [cropData addObject:[temp2 objectAtIndex:i]];
        }
    }
    
    
    self.array = systemData;
    self.cropArray = cropData;
    
    
    int j = 0;
    for(int i = 0; i < [self.array count]; i++)
    {
        BOOL theyAreEqual = [self.array[i] isEqualToString:optionsSingle.systemFinal];
        if(theyAreEqual)
        {
            j = i;
        }
    }
    [_System selectRow:j inComponent:0 animated:YES];
    
    if(![[_array objectAtIndex:[_System selectedRowInComponent:0]] isEqualToString: @"Unassigned"])
    {
        _NewSystem.enabled = NO;
        _NewSystem.backgroundColor = [UIColor lightGrayColor];
        _NewSystemCode.enabled = NO;
        _NewSystemCode.backgroundColor = [UIColor lightGrayColor];
    }
    else
    {
        _NewSystem.enabled = YES;
        _NewSystem.backgroundColor = [UIColor whiteColor];
        _NewSystemCode.enabled = YES;
        _NewSystemCode.backgroundColor = [UIColor whiteColor];
    }
    
    
    j = 0;
    for(int i = 0; i < [self.cropArray count]; i++)
    {
        BOOL theyAreEqual = [self.cropArray[i] isEqualToString:optionsSingle.cropFinal];
        if(theyAreEqual)
        {
            j = i;
        }
    }
    [_Crop selectRow:j inComponent:0 animated:YES];
    
    if(![[_cropArray objectAtIndex:[_Crop selectedRowInComponent:0]] isEqualToString: @"New Crop"])
    {
        _NewCrop.enabled = NO;
        _NewCrop.backgroundColor = [UIColor lightGrayColor];
    }
    else
    {
        _NewCrop.enabled = YES;
        _NewCrop.backgroundColor = [UIColor whiteColor];
    }
    
    self.NewSystem.delegate = self;
    self.NewSystemCode.delegate = self;
    self.NewCrop.delegate = self;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _NewCrop)
    {
        [textField resignFirstResponder];
        if([self shouldPerformSegueWithIdentifier:@"SystemToFill" sender:self])
        {
            [self performSegueWithIdentifier:@"SystemToFill" sender:self];
        }
        return NO;
    }
    return NO;
}

#pragma mark Picker Data Source Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if(pickerView.tag == 1)
    {
        return [_array count];
    }
    else
    {
        return [_cropArray count];
    }
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView.tag == 1)
    {
        return [_array objectAtIndex:row];
    }
    else
    {
        return [_cropArray objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        if(![[_array objectAtIndex:[_System selectedRowInComponent:0]] isEqualToString: @"Unassigned"])
        {
            _NewSystem.enabled = NO;
            _NewSystem.backgroundColor = [UIColor lightGrayColor];
            _NewSystemCode.enabled = NO;
            _NewSystemCode.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            _NewSystem.enabled = YES;
            _NewSystem.backgroundColor = [UIColor whiteColor];
            _NewSystemCode.enabled = YES;
            _NewSystemCode.backgroundColor = [UIColor whiteColor];
        }
    }
    else
    {
        if(![[_cropArray objectAtIndex:[_Crop selectedRowInComponent:0]] isEqualToString: @"New Crop"])
        {
            _NewCrop.enabled = NO;
            _NewCrop.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            _NewCrop.enabled = YES;
            _NewCrop.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if(([[_array objectAtIndex:[_System selectedRowInComponent:0]] isEqualToString: @"Unassigned"]) && ([_NewSystem.text length] > 0))
    {
        NSLog(@"new system = %@", _NewSystem.text);
        optionsSingle.systemFinal = _NewSystem.text;
        optionsSingle.systemCode = _NewSystemCode.text;
        
        if([[OperationsDatabase database] getSystemID] == -1)
        {
            [[OperationsDatabase database] addNewSystem];
        }
        
    }
    else
    {
        NSLog(@"spot2");
        optionsSingle.systemFinal = [_array objectAtIndex:[_System selectedRowInComponent:0]];
        
        NSArray *operationMatInfo = [[OperationsDatabase database]getSystemCode];
        for(OperationsInfo *info in operationMatInfo)
        {
            //NSLog(@"%@", info.operationsMaterials);
            optionsSingle.systemCode = info.operationsSystemCode;
        }
    }
    
    if(([[_cropArray objectAtIndex:[_Crop selectedRowInComponent:0]] isEqualToString: @"New Crop"]))
    {
        NSLog(@"spot3");
        
        optionsSingle.cropFinal = _NewCrop.text;
        
        if([[OperationsDatabase database] getCropID] == -1)
        {
            [[OperationsDatabase database] addNewCrop];
        }
    }
    else
    {
        NSLog(@"spot4");
        optionsSingle.cropFinal = [_cropArray objectAtIndex:[_Crop selectedRowInComponent:0]];
    }
    
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"SystemToFill"])
    {
        if(([[_array objectAtIndex:[_System selectedRowInComponent:0]] isEqualToString: @"Unassigned"]) && ([_NewSystem.text length] > 0))
        {
            if([self.NewSystemCode.text length] == 0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"System Error" message:@"You did not type a system code for your new system." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
                [alert show];
                return NO;
            }
            else
            {
                optionsSingle.systemFinal = _NewSystem.text;
                optionsSingle.systemCode = _NewSystemCode.text;

                if([[OperationsDatabase database] getSystemCodeID] != -1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"System Code Error" message:@"You entered a System Code that is already being used." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
                    [alert show];
                    return NO;
                }
            }
        }
        if(([[_cropArray objectAtIndex:[_Crop selectedRowInComponent:0]] isEqualToString: @"New Crop"]))
        {
            if([self.NewCrop.text length] == 0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Crop Error" message:@"You did not pick a crop." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
                [alert show];
                return NO;
            }
        }
    }
    
    return YES;
}


@end
