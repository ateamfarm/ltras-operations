//
//  TractorViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 5/6/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "TractorViewController.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface TractorViewController ()

@end

@implementation TractorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    _Description.text = optionsSingle.tractorFinal;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _Comments.layer.borderWidth = 1.5f;
//    _Comments.layer.borderColor = [[UIColor blueColor]CGColor];
    
    self.Make.delegate = self;
    self.Brand.delegate = self;
    self.Horsepower.delegate = self;
    self.Description.delegate = self;
    self.Drivetrain.delegate = self;
    self.fuelType.delegate = self;
    self.Comments.delegate = self;

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        if([self shouldPerformSegueWithIdentifier:@"SubmitTractor" sender:self])
        {
            [self performSegueWithIdentifier:@"SubmitTractor" sender:self];
        }
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _Make)
    {
        [_Brand becomeFirstResponder];
    }
    else if(textField == _Brand)
    {
        [_Horsepower becomeFirstResponder];
    }
    else if(textField == _Horsepower)
    {
        [_Description becomeFirstResponder];
    }
    else if(textField == _Description)
    {
        [_Drivetrain becomeFirstResponder];
    }
    else if(textField == _Drivetrain)
    {
        [_fuelType becomeFirstResponder];
    }
    else if(textField == _fuelType)
    {
        [_Comments becomeFirstResponder];
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    optionsSingle.tractorFinal = _Description.text;

    
    if([[segue identifier] isEqualToString:@"SubmitTractor"])
    {
        optionsSingle.tractorMake = _Make.text;
        optionsSingle.tractorHorsepower = _Horsepower.text;
        optionsSingle.tractorFuelType = _fuelType.text;
        optionsSingle.tractorDriveTrain = _Drivetrain.text;
        optionsSingle.tractorComments = _Comments.text;
        optionsSingle.tractorBrand = _Brand.text;
        optionsSingle.tractorFinal = _Description.text;
        
        if([optionsSingle.tractorDriveTrain length] == 0)
        {
            optionsSingle.tractorDriveTrain = @"4WD";
        }
        if([optionsSingle.tractorFuelType length] == 0)
        {
            optionsSingle.tractorFuelType = @"Diesel";
        }
    }
    else
    {
        optionsSingle.tractorMake = nil;
        optionsSingle.tractorHorsepower = nil;
        optionsSingle.tractorFuelType = nil;
        optionsSingle.tractorDriveTrain = nil;
        optionsSingle.tractorComments = nil;
        optionsSingle.tractorBrand = nil;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    optionsSingle.tractorFinal = _Description.text;
    
    if([identifier isEqualToString:@"SubmitTractor"])
    {
        if([_Make.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Make Error" message:@"Tractor Make was not filled out" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([_Brand.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Brand Error" message:@"Tractor Brand was not filled out" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(([_Horsepower.text length] == 0) || ([_Horsepower.text intValue] == 0 && (![_Horsepower.text isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Horsepower Error" message:@"Please enter an integer value for the horsepower" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([_Description.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Description Error" message:@"Tractor Description was not filled out" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([[OperationsDatabase database] getTractorID] != -1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tractor Error" message:@"Tractor is already in database. Please press the back button twice." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(![self connected])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Error" message:@"You do not have an internet connection." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:@"Add Locally", nil];
            
            optionsSingle.tractorFinal = _Description.text;
            optionsSingle.tractorMake = _Make.text;
            optionsSingle.tractorHorsepower = _Horsepower.text;
            optionsSingle.tractorFuelType = _fuelType.text;
            optionsSingle.tractorDriveTrain = _Drivetrain.text;
            optionsSingle.tractorComments = _Comments.text;
            optionsSingle.tractorBrand = _Brand.text;
            optionsSingle.tractorFinal = _Description.text;
            
            if([optionsSingle.tractorDriveTrain length] == 0)
            {
                optionsSingle.tractorDriveTrain = @"4WD";
            }
            if([optionsSingle.tractorFuelType length] == 0)
            {
                optionsSingle.tractorFuelType = @"Diesel";
            }
            
            [[OperationsDatabase database] addNewTractor];
            [[OperationsDatabase database] addTractorQueue];
            optionsSingle.tractorMake = nil;
            optionsSingle.tractorHorsepower = nil;
            optionsSingle.tractorFuelType = nil;
            optionsSingle.tractorDriveTrain = nil;
            optionsSingle.tractorComments = nil;
            optionsSingle.tractorBrand = nil;
            [alert show];
            return NO;
        }
        
    }
    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self performSegueWithIdentifier:@"addLocalTractor" sender:self];
    }
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
