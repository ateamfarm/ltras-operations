//
//  TractorViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 5/6/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"
#import "Reachability.h"

@interface TractorViewController : UIViewController
{
    GlobalVariables *optionsSingle;
    Reachability *internetReachableFoo;
}

//variable definitions
@property (weak, nonatomic) IBOutlet UITextField *Make;
@property (weak, nonatomic) IBOutlet UITextField *Brand;
@property (weak, nonatomic) IBOutlet UITextField *Horsepower;
@property (strong, nonatomic) IBOutlet UITextField *Description;
@property (weak, nonatomic) IBOutlet UITextField *Drivetrain;
@property (weak, nonatomic) IBOutlet UITextField *fuelType;
@property (weak, nonatomic) IBOutlet UITextView *Comments;

- (BOOL)connected;

@end
