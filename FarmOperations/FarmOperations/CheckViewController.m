//
//  CheckViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "CheckViewController.h"
#import "CheckCell.h"
#import "OperationsDatabase.h"

@interface CheckViewController ()
{
    sqlite3 *opDB;
    NSString *dbPathString;
}

@end

@implementation CheckViewController
@synthesize Title, Description;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    
    NSLog(@"System Final = %@", optionsSingle.systemFinal);
    
    [super viewDidLoad];
    [self createOrOpenDB];
    
    Title = @[@"Tractor", @"Equipment", @"Operation Type", @"Date", @"Year", @"Material Type", @"Material Description", @"Material Number", @"Material Units", @"Material Comments", @"System", @"System Code", @"Crop", @"Number of Tractor Operators", @"Number of Workers",
               @"Number of Passes", @"Tractor Operator", @"Description", @"Comments"];
    
    Description = @[optionsSingle.tractorFinal, optionsSingle.equipmentFinal, optionsSingle.opTypeFinal, optionsSingle.dateFinal, optionsSingle.yearFinal, optionsSingle.matTypeFinal, optionsSingle.matDescFinal, optionsSingle.matUnitsFinal, optionsSingle.matUnitTypeFinal, optionsSingle.matCommFinal, optionsSingle.systemFinal, optionsSingle.systemCode, optionsSingle.cropFinal, optionsSingle.tracOpFinal, optionsSingle.workFinal, optionsSingle.passFinal, optionsSingle.tracOperatorFinal, optionsSingle.descFinal, optionsSingle.commFinal];
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Title.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CheckCell";
    
    CheckCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CheckCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    int row = [indexPath row];
    
    //NSLog(@"%@", Description[row]);
    
    cell.TitleLabel.text = Title[row];
    cell.DescriptionLabel.text = Description[row];
    return cell;
}

- (void)createOrOpenDB
{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [path objectAtIndex:0];
    
    dbPathString = [docPath stringByAppendingPathComponent:@"Operations.sqlite"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:dbPathString])
    {
        const char *dbPath = [dbPathString UTF8String];
        
        if(sqlite3_open(dbPath, &opDB) == SQLITE_OK){
            sqlite3_close(opDB);
        }
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"addToQueue"])
    {
        if(sqlite3_open([dbPathString UTF8String], &opDB) == SQLITE_OK)
        {
            NSMutableArray *returnArray = [[NSMutableArray alloc] init];
            char *error;
            
            int equipmentID = [[OperationsDatabase database] getEquipmentID];
            int tractorID = [[OperationsDatabase database] getTractorID];
            int systemID = [[OperationsDatabase database] getSystemID];
            int materialsID = [[OperationsDatabase database] getOpMaterialsID];
            
            NSString *query = [NSString stringWithFormat:@"INSERT INTO PushQueue(date, operation_year, operation_type, equipment_id, equipment_description, tractor_id, tractor_description, operation_description, number_of_passes, number_of_tractor_operators, number_of_workers, operations_comments, tractor_operator, operations_material_id, material, material_description, material_comments, material_quantity, material_units, system_number, system_name, system_code, crop_type, operations_materials) VALUES ('%s', '%d', '%s', '%d', '%s', '%d', '%s', '%s', '%d', '%d', '%d', '%s', '%s', '%d', '%s', '%s', '%s', '%lf', '%s', '%d', '%s', '%s', '%s', '%s');", [optionsSingle.dateFinal UTF8String], [optionsSingle.yearFinal intValue], [optionsSingle.opTypeFinal UTF8String], equipmentID, [optionsSingle.equipmentFinal UTF8String], tractorID, [optionsSingle.tractorFinal UTF8String], [optionsSingle.descFinal UTF8String], [optionsSingle.passFinal intValue], [optionsSingle.tracOpFinal intValue], [optionsSingle.workFinal intValue], [optionsSingle.commFinal UTF8String], [optionsSingle.tracOperatorFinal UTF8String], materialsID, [optionsSingle.matTypeFinal UTF8String], [optionsSingle.matDescFinal UTF8String], [optionsSingle.matCommFinal UTF8String], [optionsSingle.matUnitsFinal doubleValue], [optionsSingle.matUnitTypeFinal UTF8String], systemID, [optionsSingle.systemFinal UTF8String], [optionsSingle.systemCode UTF8String], [optionsSingle.cropFinal UTF8String], [optionsSingle.matTypeFinal UTF8String]];
            
            NSLog(@"query = %s", [query UTF8String]);
            
            if(sqlite3_exec(opDB, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
            {
                NSLog(@"Added Operation");
                
                [returnArray addObject:optionsSingle.tractorFinal];
            }
            else{
                NSLog(@"EXEC ERROR");
            }
        }
        else{
            NSLog(@"OPEN ERROR");
        }
        sqlite3_close(opDB);
    }
}   


@end
