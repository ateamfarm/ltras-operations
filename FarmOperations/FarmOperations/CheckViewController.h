//
//  CheckViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"
#import "OperationsInfo.h"
#import "sqlite3.h"

@interface CheckViewController : UIViewController
    <UITableViewDelegate, UITableViewDataSource>
{
    GlobalVariables *optionsSingle;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *Title;
@property (nonatomic, strong) NSArray *Description;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
