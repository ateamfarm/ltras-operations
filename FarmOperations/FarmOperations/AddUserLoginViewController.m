//
//  AddUserLoginViewController.m
//  FarmOperations
//
//  Created by Student on 6/3/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "AddUserLoginViewController.h"
#import "OperationsDatabase.h"

@interface AddUserLoginViewController ()

@end

@implementation AddUserLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.username.delegate = self;
    self.password.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _username)
    {
        [_password becomeFirstResponder];
    }
    else if(textField == _password)
    {
        if([self shouldPerformSegueWithIdentifier:@"AddUser" sender:self])
        {
            [self performSegueWithIdentifier:@"AddUser" sender:self];
        }
    }
    return NO;
}

#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"AddUser"])
    {
        
        NSString *md5_pass = [[OperationsDatabase database] md5:_password.text];
        NSLog(@"md5_pass = %@", md5_pass);
        
        
        NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", _username.text, md5_pass];
        postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        NSLog(@"Login PostString:%@", postString);
        
        postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
        postString = [postString stringByReplacingOccurrencesOfString:@"" withString:@"+"];
        
        NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/login.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:data];
        NSLog(@"request:%@", request);
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        
        NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
        
        response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"Login responseString: %@",responseString);
        
        
        //NSLog(@"responseString: %@",responseString);
        if (![responseString isEqualToString:@"Success"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Unsuccessful" message:@"You entered your email or password incorrectly." delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        
    }
    return YES;
}


@end
