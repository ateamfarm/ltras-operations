//
//  SubmittedViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"

@interface SubmittedViewController : UIViewController
{
    GlobalVariables *optionsSingle;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
- (IBAction)exitApplication:(id)sender;
@end
