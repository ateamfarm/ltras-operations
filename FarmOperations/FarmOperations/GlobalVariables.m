//
//  GlobalVariables.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/23/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "GlobalVariables.h"

@implementation GlobalVariables

+(GlobalVariables *) singleObj
{
    static GlobalVariables *single = nil;

    if(!single)
    {
        single = [[super allocWithZone:nil] init];
    }
    return single;
}


+(id) allocWithZone:(struct _NSZone *)zone
{
    return [self singleObj];
}

-(id) init
{
    self = [super init];
    if (self)
    {
        _tractorFinal = self.tractorFinal;
        _equipmentFinal = self.equipmentFinal;
        _opTypeFinal = self.opTypeFinal;
        _matTypeFinal = self.matTypeFinal;
        _matDescFinal =  self.matDescFinal;
        _matCommFinal =  self.matCommFinal;
        _matUnitsFinal = self.matUnitsFinal;
        _matUnitTypeFinal = self.matUnitTypeFinal;
        _dateFinal = self.dateFinal;
        _yearFinal = self.yearFinal;
        _systemFinal = self.systemFinal;
        _cropFinal = self.cropFinal;
        _tracOpFinal = self.tracOpFinal;
        _tracOperatorFinal = self.tracOperatorFinal;
        _workFinal = self.workFinal;
        _passFinal = self.passFinal;
        _descFinal = self.descFinal;
        _commFinal = self.commFinal;
        _keepDateFinal = self.keepDateFinal;
        _systemCode = self.systemCode;
        _tractorMake = self.tractorMake;
        _tractorBrand = self.tractorBrand;
        _tractorHorsepower = self.tractorHorsepower;
        _tractorDriveTrain = self.tractorDriveTrain;
        _tractorFuelType = self.tractorFuelType;
        _tractorComments = self.tractorComments;
        _equipmentName = self.equipmentName;
        _equipmentWidth = self.equipmentWidth;
        _equipmentComments = self.equipmentComments;
        
        
    }
    return self;
}

@end
