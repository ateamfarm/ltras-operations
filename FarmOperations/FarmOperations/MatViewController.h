//
//  MatViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 5/15/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"


@protocol AutocompletionTableViewDelegate;

@interface MatViewController : UIViewController<
UIPickerViewDelegate, UIPickerViewDataSource>
{
    GlobalVariables *optionsSingle;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *Material;
@property (strong, nonatomic) IBOutlet UITextField *Description;
@property (strong, nonatomic) IBOutlet UIPickerView *MatPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *UnitPicker;
@property (strong, nonatomic) IBOutlet UITextField *MaterialUnitNumber;
@property (strong, nonatomic) IBOutlet UITextField *NewMaterial;
@property (strong, nonatomic) IBOutlet UIButton *AddMaterial;
@property (strong, nonatomic) IBOutlet UITextField *MatComments;

@end
