//
//  WebViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OperationsDatabase.h"
#import "OperationsDatabase.h"

@interface WebViewController : UIViewController
{
    GlobalVariables *optionsSingle;
}


@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
