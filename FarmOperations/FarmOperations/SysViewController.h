//
//  SysViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"


@interface SysViewController : UIViewController<
UIPickerViewDelegate, UIPickerViewDataSource>
{
    GlobalVariables *optionsSingle;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@property (strong, nonatomic) IBOutlet UIPickerView *System;
@property (strong, nonatomic) IBOutlet UIPickerView *Crop;
@property (strong, nonatomic) IBOutlet UITextField *NewSystem;
@property (strong, nonatomic) IBOutlet UITextField *NewCrop;
@property (strong, nonatomic) IBOutlet UITextField *NewSystemCode;


@end
