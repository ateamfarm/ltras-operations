//
//  SubmitCell.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/29/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "SubmitCell.h"

@implementation SubmitCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
