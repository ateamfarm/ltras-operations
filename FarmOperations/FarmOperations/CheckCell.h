//
//  CheckCell.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *DescriptionLabel;

@end
