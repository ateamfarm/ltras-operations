//
//  UpdateDBViewController.h
//  FarmOperations
//
//  Created by Student on 5/19/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "ViewController.h"

@interface UpdateDBViewController : ViewController

//Text field that stores inputted username
@property (weak, nonatomic) IBOutlet UITextField *username;
//Text field that stores inputted password
@property (weak, nonatomic) IBOutlet UITextField *password;

@end
