//
//  OperationsViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"
#import "Reachability.h"

@interface OperationsViewController : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
    GlobalVariables *optionsSingle;
    Reachability *internetReachableFoo;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *opDate;
@property (nonatomic, strong) NSMutableArray *opSystem;
@property (nonatomic, strong) NSMutableArray *opTractor;
@property (nonatomic, strong) NSMutableArray *opID;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
- (BOOL)connected;


@end
