//
//  EquipmentViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 5/6/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "EquipmentViewController.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface EquipmentViewController ()

@end

@implementation EquipmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    _Description.text = optionsSingle.equipmentFinal;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _Comments.layer.borderWidth = 1.5f;
//    _Comments.layer.borderColor = [[UIColor blueColor]CGColor];
    
    self.Name.delegate = self;
    self.Width.delegate = self;
    self.Description.delegate = self;
    self.Comments.delegate = self;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        if([self shouldPerformSegueWithIdentifier:@"SubmitEquipment" sender:self])
        {
            [self performSegueWithIdentifier:@"SubmitEquipment" sender:self];
        }
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _Name)
    {
        [_Width becomeFirstResponder];
    }
    else if(textField == _Width)
    {
        [_Description becomeFirstResponder];
    }
    else if(textField == _Description)
    {
        [_Comments becomeFirstResponder];
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    optionsSingle.equipmentFinal = _Description.text;
    
    if([[segue identifier] isEqualToString:@"SubmitEquipment"])
    {
        optionsSingle.equipmentComments = _Comments.text;
        optionsSingle.equipmentName = _Name.text;
        optionsSingle.equipmentWidth = _Width.text;
    }
    else
    {
        optionsSingle.equipmentComments = nil;
        optionsSingle.equipmentName = nil;
        optionsSingle.equipmentWidth = nil;
    }
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    optionsSingle.equipmentFinal = _Description.text;
    
    if([identifier isEqualToString:@"SubmitEquipment"])
    {
        if([_Name.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Equipment Name Error" message:@"Equipment Name was not filled out" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(([_Width.text length] == 0) || ([_Width.text doubleValue] == 0 && (![_Width.text isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Equipment Width Error" message:@"Please enter a decimal in feet for the width" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([_Description.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Equipment Description Error" message:@"Equipment Description was not filled out" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([[OperationsDatabase database] getEquipmentID] != -1)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Equipment Error" message:@"Equipment is already in database. Please press the back button twice." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(![self connected])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Error" message:@"You do not have an internet connection." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:@"Add Locally", nil];
            [alert show];
            optionsSingle.equipmentFinal = _Description.text;
            optionsSingle.equipmentComments = _Comments.text;
            optionsSingle.equipmentName = _Name.text;
            optionsSingle.equipmentWidth = _Width.text;
            [[OperationsDatabase database] addNewEquipment];
            [[OperationsDatabase database] addEquipmentQueue];
            return NO;
        }
    }
    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self performSegueWithIdentifier:@"addLocalEquipment" sender:self];
    }
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
