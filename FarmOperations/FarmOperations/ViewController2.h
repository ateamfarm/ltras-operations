//
//  ViewController2.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"

@protocol AutocompletionTableViewDelegate;

@interface ViewController2 : UIViewController
{
    GlobalVariables *optionsSingle;
}

@property (strong, nonatomic) IBOutlet UITextField *Equipment;

@end
