//
//  WebViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    [super viewDidLoad];

    self.username.delegate = self;
    self.password.delegate = self;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _username)
    {
        [_password becomeFirstResponder];
    }
    else if(textField == _password)
    {
        if([self shouldPerformSegueWithIdentifier:@"SubmitAll" sender:self])
        {
            [self performSegueWithIdentifier:@"SubmitAll" sender:self];
        }
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"SubmitAll"])
    {
        optionsSingle.tractorFinal = nil;
        optionsSingle.equipmentFinal = nil;
        optionsSingle.opTypeFinal = nil;
        optionsSingle.matTypeFinal = nil;
        optionsSingle.matDescFinal = nil;
        optionsSingle.matUnitsFinal = nil;
        optionsSingle.matUnitTypeFinal = nil;
        optionsSingle.dateFinal = nil;
        optionsSingle.yearFinal = nil;
        optionsSingle.systemFinal = nil;
        optionsSingle.tracOpFinal = nil;
        optionsSingle.workFinal = nil;
        optionsSingle.passFinal = nil;
        optionsSingle.descFinal = nil;
        optionsSingle.commFinal = nil;
        optionsSingle.keepDateFinal = nil;
        optionsSingle.cropFinal = nil;
        optionsSingle.systemCode = nil;
        optionsSingle.tractorMake = nil;
        optionsSingle.tractorBrand = nil;
        optionsSingle.tractorHorsepower = nil;
        optionsSingle.tractorDriveTrain = nil;
        optionsSingle.tractorFuelType = nil;
        optionsSingle.tractorComments = nil;
        optionsSingle.equipmentName = nil;
        optionsSingle.equipmentWidth = nil;
        optionsSingle.equipmentComments = nil;
        optionsSingle.tracOperatorFinal = nil;
        
        NSLog(@"about to call submitJobs from webViewController.");
        if([[OperationsDatabase database] submitJobs])
        {
            [[OperationsDatabase database] deletePushes];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Transaction failed." delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"SubmitAll"])
    {
        NSString *md5_pass = [[OperationsDatabase database] md5:_password.text];
        NSLog(@"md5_pass = %@", md5_pass);
        
        
        NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", _username.text, md5_pass];
        postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        NSLog(@"Login PostString:%@", postString);
        
        postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
        postString = [postString stringByReplacingOccurrencesOfString:@"" withString:@"+"];
        
        NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/login.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:data];
        NSLog(@"request:%@", request);
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        
        NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
        
        response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"Login responseString: %@",responseString);
        
        
        //NSLog(@"responseString: %@",responseString);
        if (![responseString isEqualToString:@"Success"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Unsuccessful" message:@"You entered your email or password incorrectly." delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        
    }
    return YES;

}


@end
