//
//  SubmittedViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "SubmittedViewController.h"

@interface SubmittedViewController ()

@end

@implementation SubmittedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exitApplication:(id)sender
{
    exit(0);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"StartNew"])
    {
        optionsSingle.tractorFinal = nil;
        optionsSingle.equipmentFinal = nil;
        optionsSingle.opTypeFinal = nil;
        optionsSingle.matTypeFinal = nil;
        optionsSingle.matDescFinal = nil;
        optionsSingle.matUnitsFinal = nil;
        optionsSingle.matUnitTypeFinal = nil;
        optionsSingle.dateFinal = nil;
        optionsSingle.yearFinal = nil;
        optionsSingle.systemFinal = nil;
        optionsSingle.tracOpFinal = nil;
        optionsSingle.workFinal = nil;
        optionsSingle.passFinal = nil;
        optionsSingle.descFinal = nil;
        optionsSingle.commFinal = nil;
        optionsSingle.keepDateFinal = nil;
        optionsSingle.cropFinal = nil;
        optionsSingle.systemCode = nil;
        optionsSingle.tractorMake = nil;
        optionsSingle.tractorBrand = nil;
        optionsSingle.tractorHorsepower = nil;
        optionsSingle.tractorDriveTrain = nil;
        optionsSingle.tractorFuelType = nil;
        optionsSingle.tractorComments = nil;
        optionsSingle.equipmentName = nil;
        optionsSingle.equipmentWidth = nil;
        optionsSingle.equipmentComments = nil;
        optionsSingle.tracOperatorFinal = nil;
        
    }
}

@end
