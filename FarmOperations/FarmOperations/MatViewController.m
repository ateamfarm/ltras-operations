//
//  MatViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 5/15/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "MatViewController.h"
#import "AutocompletionTableView.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface MatViewController ()

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UILabel *sampleLabel;
@property (strong, nonatomic) NSArray *array;
@property (strong, nonatomic) NSArray *units;

@end

@implementation MatViewController

@synthesize textField = _textField;
@synthesize sampleLabel = _sampleLabel;
@synthesize autoCompleter = _autoCompleter;

- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.textField inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        
        
        NSArray *operationTypesInfo = [[OperationsDatabase database] getAllMatDescriptions];
        NSMutableArray *opTypeData;
        for(OperationsInfo *info in operationTypesInfo)
        {
            NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsMaterialsDesc, nil];
            opTypeData = [NSMutableArray arrayWithArray:opTypeData];
            [opTypeData addObjectsFromArray:temp];
        }
        
        NSArray *temp = [opTypeData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        _autoCompleter.suggestionsDictionary = temp;
    }
    return _autoCompleter;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    
    self.Description.text = optionsSingle.matDescFinal;
    self.MaterialUnitNumber.text = optionsSingle.matUnitsFinal;
    self.MatComments.text = optionsSingle.matCommFinal;
    
    [super viewDidLoad];
    //NSLog(@"tractor=%@, ", optionsSingle.tractorFinal);
    
    NSArray *operationMatInfo = [[OperationsDatabase database] getAllMaterials];
    NSMutableArray *materialData;
    for(OperationsInfo *info in operationMatInfo)
    {
        //NSLog(@"%@", info.operationsMaterials);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsMaterials, nil];
        materialData = [NSMutableArray arrayWithArray:materialData];
        [materialData addObjectsFromArray:temp];
    }
    
    NSArray *operationMatUnits = [[OperationsDatabase database] getAllUnits];
    NSMutableArray *unitsData;
    for(OperationsInfo *info in operationMatUnits)
    {
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsUnits, nil];
        unitsData = [NSMutableArray arrayWithArray:unitsData];
        [unitsData addObjectsFromArray:temp];
    }
    
    // set "value changed" event handler for TextField
    [self.textField addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    NSArray *temp = [materialData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *temp2 = [unitsData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    NSMutableArray *matData = [[NSMutableArray alloc] init];
    [matData addObject:@"no_input"];
    for(int i = 0; i < [temp count]; i++)
    {
        if(![[temp objectAtIndex:i] isEqualToString:@"no_input"])
        {
            [matData addObject:[temp objectAtIndex:i]];
        }
        
    }
    
    NSMutableArray *unitData = [[NSMutableArray alloc] init];
    [unitData addObject:@"no_units"];
    for(int i = 0; i < [temp2 count]; i++)
    {
        if(![[temp2 objectAtIndex:i] isEqualToString:@"no_units"])
        {
            [unitData addObject:[temp2 objectAtIndex:i]];
        }
    }
    
    self.array = matData;
    self.units = unitData;
    
    int j = 0;
    for(int i = 0; i < [self.array count]; i++)
    {
        BOOL theyAreEqual = [self.array[i] isEqualToString:optionsSingle.matTypeFinal];
        if(theyAreEqual)
        {
            j = i;
        }
    }
    [_MatPicker selectRow:j inComponent:0 animated:YES];
    
    
    if(![[_array objectAtIndex:[_MatPicker selectedRowInComponent:0]] isEqualToString: @"no_input"])
    {
        _NewMaterial.enabled = NO;
        _NewMaterial.backgroundColor = [UIColor lightGrayColor];
        [_AddMaterial setHidden:YES];
    }
    else
    {
        _NewMaterial.enabled = YES;
        _NewMaterial.backgroundColor = [UIColor whiteColor];
        [_AddMaterial setHidden:NO];
    }
    
    
    j = 0;
    for(int i = 0; i < [self.units count]; i++)
    {
        BOOL theyAreEqual = [self.units[i] isEqualToString:optionsSingle.matUnitTypeFinal];
        if(theyAreEqual)
        {
            j = i;
        }
    }
    [_UnitPicker selectRow:j inComponent:0 animated:YES];
    self.Material.delegate = self;
    self.NewMaterial.delegate = self;
    self.MaterialUnitNumber.delegate = self;
    self.MatComments.delegate = self;
    self.Description.delegate = self;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _NewMaterial)
    {
        [_MaterialUnitNumber becomeFirstResponder];
    }
    else if(textField == _MaterialUnitNumber)
    {
        [_MatComments becomeFirstResponder];
    }
    else if(textField == _MatComments)
    {
        [_Description becomeFirstResponder];
    }
    else if(textField == _Description)
    {
        [textField resignFirstResponder];
        if([self shouldPerformSegueWithIdentifier:@"matToSys" sender:self])
        {
            [self performSegueWithIdentifier:@"matToSys" sender:self];
        }
        return NO;
    }
    return NO;
}

- (void)viewDidUnload {
    [self setSampleLabel:nil];
    [self setSampleLabel:nil];
    [super viewDidUnload];
}

#pragma mark Picker Data Source Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag == 1)
    {
        return [_array count];
    }
    else
    {
        return [_units count];
    }
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView.tag == 1)
    {
        return [_array objectAtIndex:row];
    }
    else
    {
        return [_units objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        NSLog(@"mat = %@", [_array objectAtIndex:[_MatPicker selectedRowInComponent:0]]);
        if(![[_array objectAtIndex:[_MatPicker selectedRowInComponent:0]] isEqualToString: @"no_input"])
        {
            _NewMaterial.enabled = NO;
            _NewMaterial.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            _NewMaterial.enabled = YES;
            _NewMaterial.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AutoCompleteTableViewDelegate

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    
    NSArray *operationTypesInfo = [[OperationsDatabase database] getAllMatDescriptions];
    NSMutableArray *opTypeData;
    for(OperationsInfo *info in operationTypesInfo)
    {
        //NSLog(@"%@", info.operationsType);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsMaterialsDesc, nil];
        opTypeData = [NSMutableArray arrayWithArray:opTypeData];
        [opTypeData addObjectsFromArray:temp];
    }
    
    NSArray *temp = [opTypeData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return temp;
}

- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    //NSLog(@"%@ - Suggestion chosen: %d", completer, index);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSLog(@"mat = %@", [_array objectAtIndex:[_MatPicker selectedRowInComponent:0]]);
    if(([[_array objectAtIndex:[_MatPicker selectedRowInComponent:0]] isEqualToString: @"no_input"]) && ([_NewMaterial.text length] > 0))
    {
        
        optionsSingle.matTypeFinal = _NewMaterial.text;
        
        NSLog(@"new material %d", [[OperationsDatabase database] getMaterialsID]);
        
        if([[OperationsDatabase database] getMaterialsID] == -1)
        {
            [[OperationsDatabase database] addNewMaterial];
        }
    }
    else
    {
        NSLog(@"old material");
        optionsSingle.matTypeFinal = [_array objectAtIndex:[_MatPicker selectedRowInComponent:0]];
    }
    
    optionsSingle.matUnitTypeFinal = [_units objectAtIndex:[_UnitPicker selectedRowInComponent:0]];
    optionsSingle.matDescFinal = self.Description.text;
    optionsSingle.matUnitsFinal = self.MaterialUnitNumber.text;
    optionsSingle.matCommFinal = self.MatComments.text;
    
    //Pull database operations and see if the operation is in the database already.
    NSArray *operationTypesInfo = [[OperationsDatabase database] getAllMatDescriptions];
    NSMutableArray *opTypeData;
    for(OperationsInfo *info in operationTypesInfo)
    {
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsMaterialsDesc, nil];
        opTypeData = [NSMutableArray arrayWithArray:opTypeData];
        [opTypeData addObjectsFromArray:temp];
    }
    NSArray* compare = opTypeData;
    
    //if operation description is new push to database
    BOOL j = false;
    for(int i = 0; i < [compare count]; i++)
    {
        BOOL theyAreEqual = [compare[i] isEqualToString:optionsSingle.matDescFinal];
        if(theyAreEqual)
        {
            j = true;
        }
    }
    
    if(j == false)
    {
        [[OperationsDatabase database] addNewMaterialDesc];
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"matToSys"])
    {
        if([self.Description.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Material Description Error" message:@"You did not insert a material description." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if([self.MaterialUnitNumber.text length] == 0 || ([self.MaterialUnitNumber.text doubleValue] == 0 && (![self.MaterialUnitNumber.text isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Material Unit Number Error" message:@"You're material units are not a decimal value." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

@end
