//
//  OperationsInfo.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/22/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OperationsInfo : NSObject
{
    NSString *operationsTractor;
    NSString *operationsEquipment;
    NSString *operationsType;
    NSString *operationsMaterials;
    NSString *operationsMaterialsDesc;
    NSString *operationsSystem;
    NSString *operationsUnits;
    NSString *operationsCrop;
    NSString *finalDates;
    NSString *finalTractors;
    NSString *finalSystems;
    NSString *operationsSystemCode;
    NSString *finalID;
}

@property(nonatomic, copy) NSString *operationsTractor;
@property(nonatomic, copy) NSString *operationsEquipment;
@property(nonatomic, copy) NSString *operationsType;
@property(nonatomic, copy) NSString *operationsMaterials;
@property(nonatomic, copy) NSString *operationsMaterialsDesc;
@property(nonatomic, copy) NSString *operationsSystem;
@property(nonatomic, copy) NSString *operationsUnits;
@property(nonatomic, copy) NSString *operationsCrop;
@property(nonatomic, copy) NSString *finalDates;
@property(nonatomic, copy) NSString *finalTractors;
@property(nonatomic, copy) NSString *finalSystems;
@property(nonatomic, copy) NSString *operationsSystemCode;
@property(nonatomic, copy) NSString *finalID;

-(id) initMaterials:(NSString *)operationsMaterials;

-(id) initMaterialsDesc:(NSString *)operationsMaterialsDesc;

-(id) initOperationType:(NSString *)operationsType;

-(id) initSystem:(NSString *)operationsSystem;

-(id) initEquipment:(NSString *)operationsEquipment;

-(id) initTractor:(NSString *)operationsTractor;

-(id) initUnits:(NSString *)operationsUnits;

-(id) initCrop:(NSString *)operationsCrop;

-(id) initFinalDates:(NSString *)finalDates;

-(id) initFinalTractors:(NSString *)finalTractors;

-(id) initFinalSystems:(NSString *)finalSystems;

-(id) initSystemCode:(NSString *)operationsSystemCode;

-(id) initFinalID:(NSString *)finalID;

@end
