//
//  OperationsInfo.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/22/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "OperationsInfo.h"

@implementation OperationsInfo

@synthesize operationsTractor, operationsEquipment, operationsType, operationsMaterials,
    operationsSystem, operationsCrop, operationsUnits, operationsMaterialsDesc;

-(id) initMaterials:(NSString *)operationsMaterials
{
    self = [super init];
    if(self)
    {
        self.operationsMaterials = operationsMaterials;
    }
    return self;
}

-(id) initMaterialsDesc:(NSString *)operationsMaterialsDesc
{
    self = [super init];
    if(self)
    {
        self.operationsMaterialsDesc = operationsMaterialsDesc;
    }
    return self;
}

-(id) initOperationType:(NSString *)operationsType
{
    self = [super init];
    if(self)
    {
        self.operationsType = operationsType;
    }
    return self;
}

-(id) initSystem:(NSString *)operationsSystem
{
    self = [super init];
    if(self)
    {
        self.operationsSystem = operationsSystem;
    }
    return self;
}

-(id) initEquipment:(NSString *)operationsEquipment
{
    self = [super init];
    if(self)
    {
        self.operationsEquipment = operationsEquipment;
    }
    return self;
}

-(id) initTractor:(NSString *)operationsTractor
{
    self = [super init];
    if(self)
    {
        self.operationsTractor = operationsTractor;
    }
    return self;
}

-(id) initUnits:(NSString *)operationsUnits
{
    self = [super init];
    if(self)
    {
        self.operationsUnits = operationsUnits;
    }
    return self;
}

-(id) initCrop:(NSString *)operationsCrop
{
    self = [super init];
    if(self)
    {
        self.operationsCrop = operationsCrop;
    }
    return self;
}

-(id) initFinalDates:(NSString *)finalDates
{
    self = [super init];
    if(self)
    {
        self.finalDates = finalDates;
    }
    return self;
}

-(id) initFinalTractors:(NSString *)finalTractors
{
    self = [super init];
    if(self)
    {
        self.finalTractors = finalTractors;
    }
    return self;
}

-(id) initFinalSystems:(NSString *)finalSystems
{
    self = [super init];
    if(self)
    {
        self.finalSystems = finalSystems;
    }
    return self;
}

-(id) initSystemCode:(NSString *)operationsSystemCode
{
    self = [super init];
    if(self)
    {
        self.operationsSystemCode = operationsSystemCode;
    }
    return self;
}

-(id) initFinalID:(NSString *)finalID
{
    self = [super init];
    if(self)
    {
        self.finalID = finalID;
    }
    return self;
}

@end
