//
//  OperationsDatabase.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/22/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "GlobalVariables.h"

@interface OperationsDatabase : NSObject
{
    sqlite3 *database;
    NSString *dbPathString;
    GlobalVariables *optionsSingle;
}

+(OperationsDatabase *) database;

- (void)createEditableCopyOfDatabaseIfNeeded;
-(NSArray *)getAllMaterials;
-(NSArray *)getAllOpTypes;
-(NSArray *)getAllMatDescriptions;
-(NSArray *)getAllSystems;
-(NSArray *)getAllEquipment;
-(NSArray *)getAllTractors;
-(int)getPickerMaterials;
-(int)getPickerSystems;
-(NSArray *)getAllUnits;
-(NSArray *)getAllCrops;
-(int)getPickerSystemsSpot;
-(int)getPickerCropSpot;
-(int)getPickerMatTypeSpot;
-(int)getPickerMatUnitsSpot;
-(void)createOrOpenDB;
-(void)addNewTractor;
-(void)addNewEquipment;
-(void)addNewOpType;
-(void)addNewMaterial;
-(void)addNewMaterialDesc;
-(void)addNewSystem;
-(void)addNewCrop;
-(int)getEquipmentID;
-(int)getTractorID;
-(int)getSystemID;
-(int)getSystemCodeID;
-(int)getMaterialsID;
-(int)getCropID;
-(int)getOpMaterialsID;
-(NSArray *)getAllFinalSystems;
-(NSArray *)getAllFinalTractors;
-(NSArray *)getAllDates;
-(NSArray *)getSystemCode;
-(void)deletePushes;
-(NSArray *)getAllFinalID;
-(void)deleteByID:(NSMutableArray *) ID;
-(BOOL)submitJobs;
-(void)getNewDB;
-(void)removeDB;
- (NSString *) addSlashForQuotes: (NSString *) test;
-(void)updateTractors;
-(void)updateEquipment;
-(void)updateCropTypes;
-(void)updateOperationMaterials;
-(void)updateOperationTypes;
-(void)updateOperationUnits;
-(void)updateLookupOperationMaterials;
-(void)updateSystemNames;
-(void)executeQuery:(NSString *)query;
-(BOOL)submitTractorQueue;
-(BOOL)submitEquipmentQueue;
-(void)addEquipmentQueue;
-(void)addTractorQueue;
- (NSString *) md5:(NSString *) input;

@end
