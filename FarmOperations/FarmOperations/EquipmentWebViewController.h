//
//  EquipmentWebViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 5/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"

@interface EquipmentWebViewController : UIViewController
{
    GlobalVariables *optionsSingle;
}

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end
