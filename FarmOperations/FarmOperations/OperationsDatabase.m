//
//  OperationsDatabase.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/22/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "OperationsDatabase.h"
#import "OperationsInfo.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>

@implementation OperationsDatabase

static OperationsDatabase *database;

+(OperationsDatabase *) database
{
    if(database == nil)
    {
        database = [[OperationsDatabase alloc] init];
    }
    return database;
}


-(id) init
{
    self = [super init];
    if(self)
    {
        [self createEditableCopyOfDatabaseIfNeeded];
        [self createOrOpenDB];
        optionsSingle = [GlobalVariables singleObj];
    }
    return self;
}

- (void)createOrOpenDB
{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [path objectAtIndex:0];
    
    dbPathString = [docPath stringByAppendingPathComponent:@"Operations.sqlite"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:dbPathString])
    {
        const char *dbPath = [dbPathString UTF8String];
        
        if(sqlite3_open(dbPath, &database) == SQLITE_OK){
            sqlite3_close(database);
        }
    }
    
}

- (void)createEditableCopyOfDatabaseIfNeeded
{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"Operations.sqlite"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    // NSLog(@"path : %@", writableDBPath);
    if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Operations.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success)
    {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}


-(NSArray *)getAllMaterials
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT operations_materials FROM lookup_operation_materials";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *materialGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *mat = [[NSString alloc] initWithUTF8String:materialGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initMaterials:mat];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    return returnArray;
}

-(NSArray *)getAllOpTypes
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT operation_type FROM lookup_operation_types;";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *opTypeGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *opType = [[NSString alloc] initWithUTF8String:opTypeGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initOperationType:opType];
                [returnArray addObject:info];
            
            }
        
        sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
   
    return returnArray;
}

-(NSArray *)getAllMatDescriptions
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT description FROM operation_materials;";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *opTypeGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *opType = [[NSString alloc] initWithUTF8String:opTypeGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initMaterialsDesc:opType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllSystems
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT system_name FROM system_names";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *systemGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *systemType = [[NSString alloc] initWithUTF8String:systemGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initSystem:systemType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllEquipment
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT description FROM equipment";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *equipmentGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *equipmentType = [[NSString alloc] initWithUTF8String:equipmentGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initEquipment:equipmentType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllTractors
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT description FROM tractors";
    sqlite3_stmt *statement;
    
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *tractorGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *tractorType = [[NSString alloc] initWithUTF8String:tractorGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initTractor:tractorType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}


-(int)getPickerMaterials
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT operations_materials FROM lookup_operation_materials";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *materialGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *mat = [[NSString alloc] initWithUTF8String:materialGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initMaterials:mat];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
        
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return 0;
}

-(int)getPickerSystems
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT system_name FROM system_names";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *systemGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *systemType = [[NSString alloc] initWithUTF8String:systemGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initSystem:systemType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return 0;
}

-(NSArray *)getAllUnits
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT operation_units FROM lookup_operation_units";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *unitsGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *unitsType = [[NSString alloc] initWithUTF8String:unitsGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initUnits:unitsType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllCrops
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT crop_type FROM lookup_crop_types";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *cropGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *cropType = [[NSString alloc] initWithUTF8String:cropGroup];
            
                OperationsInfo *info = [[OperationsInfo alloc] initCrop:cropType];
                [returnArray addObject:info];
            
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(int)getPickerSystemsSpot
{
    int row = 0;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.systemFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT system_number FROM system_names WHERE system_name = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getPickerCropSpot
{
    int row = 0;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.cropFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT crop_type_id FROM lookup_crop_types WHERE crop_type = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getPickerMatTypeSpot
{
    int row = 0;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.matTypeFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT operation_material_id FROM lookup_operation_materials WHERE operations_materials = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getPickerMatUnitsSpot
{
    int row = 0;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.matUnitTypeFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT material_unit_id FROM lookup_units WHERE material_units = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
        
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(void)addNewTractor
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSMutableArray *returnArray = [[NSMutableArray alloc] init];
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.tractorFinal];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO tractors(description) VALUES (\"%s\");", [temp UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
            
            [returnArray addObject:optionsSingle.tractorFinal];
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else{
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);

}

-(void)addNewEquipment
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSMutableArray *returnArray = [[NSMutableArray alloc] init];
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.equipmentFinal];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO equipment(description) VALUES (\"%s\");", [temp UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
            
            [returnArray addObject:optionsSingle.tractorFinal];
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else{
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
}

-(void)addNewOpType
{
    //find a way to stop it from adding if it is already in the database
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.opTypeFinal];
            
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_operation_types(operation_type) VALUES (\"%s\");", [temp UTF8String]];
            
        NSLog(@"query = %s", [query UTF8String]);
            
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
    }
    
    else
    {
        NSLog(@"OPEN ERROR");
    }
    
    sqlite3_close(database);
}

-(void)addNewMaterial
{
    int i = [self getPickerMatTypeSpot];
    NSLog(@"%d", i);
    if((sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK) && (i == 0))
    {
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.matTypeFinal];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_operation_materials(operations_materials) VALUES (\"%s\");", [temp UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
    }
    
    else
    {
        NSLog(@"OPEN ERROR");
    }
    
    sqlite3_close(database);
}

-(void)addNewMaterialDesc
{
    //find a way to stop it from adding if it is already in the database
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.matDescFinal];
        NSString *temp2 = [self addSlashForQuotes:optionsSingle.matTypeFinal];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO operation_materials(material, description) VALUES (\"%s\", \"%s\");", [temp2 UTF8String], [temp UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
    }
    
    else
    {
        NSLog(@"OPEN ERROR");
    }
    
    sqlite3_close(database);
}

-(void)addNewSystem
{
    int i = [self getPickerSystemsSpot];
    NSLog(@"%d", i);
    if((sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK) && (i == 0))
    {
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.systemFinal];
        NSString *temp2 = [self addSlashForQuotes:optionsSingle.systemCode];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO system_names(system_name, system_code) VALUES (\"%s\", \"%s\");", [temp UTF8String], [temp2 UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
    }
    
    else
    {
        NSLog(@"OPEN ERROR");
    }
    
    sqlite3_close(database);
}

-(void)addNewCrop
{
    int i = [self getPickerCropSpot];
    NSLog(@"%d", i);
    if((sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK) && (i == 0))
    {
        char *error;
        
        NSString *temp = [self addSlashForQuotes:optionsSingle.cropFinal];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_crop_types(crop_type) VALUES (\"%s\");", [temp UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
    }
    
    else
    {
        NSLog(@"OPEN ERROR");
    }
    
    sqlite3_close(database);
}

-(NSArray *)getAllDates
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT date FROM PushQueue";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *dateGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *dateType = [[NSString alloc] initWithUTF8String:dateGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initFinalDates:dateType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllFinalTractors
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT tractor_description FROM PushQueue";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *cropGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *cropType = [[NSString alloc] initWithUTF8String:cropGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initFinalTractors:cropType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getAllFinalSystems
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT system_name FROM PushQueue";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *cropGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *cropType = [[NSString alloc] initWithUTF8String:cropGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initFinalSystems:cropType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(NSArray *)getSystemCode
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.systemFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT system_code FROM system_names WHERE system_name = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *cropGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *cropType = [[NSString alloc] initWithUTF8String:cropGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initSystemCode:cropType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    NSLog(@"array = %@", returnArray);
    
    return returnArray;
}

-(void)deletePushes
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *query = [NSString stringWithFormat:@"DELETE FROM PushQueue"];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
}

-(void)deleteByID:(NSMutableArray *) ID
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;

        NSString *query = [NSString stringWithFormat:@"DELETE FROM PushQueue WHERE ID = %@", ID];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
}

-(NSArray *)getAllFinalID
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT ID FROM PushQueue";
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *cropGroup = (char *)sqlite3_column_text(statement, 0);
                NSString *cropType = [[NSString alloc] initWithUTF8String:cropGroup];
                
                OperationsInfo *info = [[OperationsInfo alloc] initFinalID:cropType];
                [returnArray addObject:info];
                
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return returnArray;
}

-(int)getEquipmentID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.equipmentFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT equipment_id FROM equipment WHERE description = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    NSLog(@"query = %@", query);
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getTractorID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.tractorFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT tractor_id FROM tractors WHERE description = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getSystemID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.systemFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT system_number FROM system_names WHERE system_name = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getSystemCodeID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.systemCode];
    
    NSString *query = [NSString stringWithFormat:@"SELECT system_number FROM system_names WHERE system_code = \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getOpMaterialsID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.matDescFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT operation_materials_id FROM operation_materials WHERE description =  \"%s\";", [temp UTF8String]];
    
    NSLog(@"query = %s", [query UTF8String]);
    
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getMaterialsID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.matTypeFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT material_id FROM lookup_operation_materials WHERE operations_materials =  \"%s\";", [temp UTF8String]];
    
    NSLog(@"query = %s", [query UTF8String]);
    
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(int)getCropID
{
    int row = -1;
    
    NSString *temp = [self addSlashForQuotes:optionsSingle.cropFinal];
    
    NSString *query = [NSString stringWithFormat:@"SELECT crop_id FROM lookup_crop_types WHERE crop_type =  \"%s\";", [temp UTF8String]];
    sqlite3_stmt *statement;
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                row = (int)sqlite3_column_int(statement, 0);
            }
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return row;
}

-(BOOL)submitJobs
{
    //TODO: Cameron Add new columns, reference the CHECKVIEWCONTROLLER page to see what needs to be sent. Or open up local sqlite db
   
    BOOL flag = FALSE;
    
    if(![[OperationsDatabase database] submitEquipmentQueue])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"New equipment was not added because of a server error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        flag = TRUE;
    }
    
    if(![[OperationsDatabase database] submitTractorQueue])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"New tractor was not added because of a server error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        flag = TRUE;
    }
    
    if(flag == TRUE)
    {
        return FALSE;
    }
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSString *sqlStatement = [NSString stringWithFormat:@"Select * from PushQueue"];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                
                NSString *_date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSString *_year = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *_operationType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *_equipmentID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *_tractorID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *_operationDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                NSString *_numPasses = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                NSString *_numTractorOps = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                NSString *_numWorkers = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *_operationComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *_tractorOperator = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *_operationMaterialsID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *_materialType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *_materialDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *_materialComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *_materialQuantity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                NSString *_materialUnits = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
                NSString *_systemNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
                NSString *_cropType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                
                
                NSString *postString = [NSString stringWithFormat:@"TractorID=%@&EquipmentID=%@&OperationType=%@&Date=%@&OperationYear=%@&OperationMaterialsID=%@&MaterialOPName=%@&MaterialOPDescription=%@&MaterialQuantity=%@&MaterialUnits=%@&MaterialComments=%@&SystemNumber=%@&CropType=%@&NumberOfTractorOperators=%@&NumberOfWorkers=%@&NumberOfPasses=%@&OperationComments=%@&OperationDescription=%@&TractorOperator=%@&crop_type=%@&operation_type=%@", _tractorID, _equipmentID, _operationType, _date, _year, _operationMaterialsID, _materialType, _materialDescription, _materialQuantity, _materialUnits, _materialComments, _systemNumber, _cropType, _numTractorOps, _numWorkers, _numPasses, _operationComments, _operationDescription, _tractorOperator, _cropType, _operationType];
                postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
                postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                
                NSLog(@"PostString:%@", postString);
                
                NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
                NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/add_farm_operation.php"];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
                [request setHTTPBody:data];
                NSLog(@"request:%@", request);
                
                NSError *requestError;
                NSURLResponse *urlResponse = nil;
                
                
                NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
                NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
                
//                responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
//                responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
//                responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
//                responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
//                responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
                
                response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"SubmitJobs responseString: %@",responseString);
                
                
                
                //NSLog(@"responseString: %@",responseString);
                if (![responseString isEqualToString:@"Success"]) {
                    NSLog(@"ResponseString was != to Success");
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"Operation was not added because of a server error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    return FALSE;
                    
                    
                }
            }
        }
    }
    return TRUE;
}


-(BOOL)submitTractorQueue{
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSString *sqlStatement = [NSString stringWithFormat:@"Select * from TractorQueue"];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *_tractorMake = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSLog(@"tractorMake = %@", _tractorMake);
                NSString *_tractortBrand = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *_tractorHorsePower = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *_tractorDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *_tractorDriveType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *_tractorFuelType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *_tractorComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                
                if ([_tractorComments length] == 0) {
                    _tractorComments = @"+";
                }
                
                
                 NSString *postString = [NSString stringWithFormat:@"TractorMake=%@&TractorBrand=%@&TractorHorsepower=%@&TractorDescription=%@&TractorDriveType=%@&TractorFuelType=%@&TractorComments=%@", _tractorMake, _tractortBrand, _tractorHorsePower, _tractorDescription, _tractorDriveType, _tractorFuelType, _tractorComments];
                
                NSLog(@"tractorqueue poststring: %@", postString);
                
                postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
                postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                
                NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
                NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/add_tractor.php"];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
                [request setHTTPBody:data];
                NSLog(@"request:%@", request);
                
                NSError *requestError;
                NSURLResponse *urlResponse = nil;
                
                
                NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
                NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
                
                response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"SubmitTractorQueue responseString: %@",responseString);
                
                
                
                //NSLog(@"responseString: %@",responseString);
                if (![responseString isEqualToString:@"Success"]) {
                    return FALSE;
                }
            }
        }
    }
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *query = [NSString stringWithFormat:@"DELETE FROM TractorQueue"];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return TRUE;
}

-(BOOL)submitEquipmentQueue{
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSString *sqlStatement = [NSString stringWithFormat:@"Select * from EquipmentQueue"];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, [sqlStatement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *_equipmentName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSString *_equipmentWidth = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *_equipmentDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *_equipmentComments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                
                if ([_equipmentComments length] == 0) {
                    _equipmentComments = @"+";
                }
                
                NSString *postString = [NSString stringWithFormat:@"EquipmentName=%@&EquipmentWidth=%@&EquipmentDescription=%@&EquipmentComments=%@",  _equipmentName, _equipmentWidth, _equipmentDescription, _equipmentComments];
                postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                
                NSLog(@"EquipmentQueue PostString:%@", postString);
                
                postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
                postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                
                NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
                NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/add_equipment.php"];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
                [request setHTTPBody:data];
                NSLog(@"request:%@", request);
                
                NSError *requestError;
                NSURLResponse *urlResponse = nil;
                
                
                NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
                NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
                
                response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"SubmitEquipmentQueue responseString: %@",responseString);
                
                
                //NSLog(@"responseString: %@",responseString);
                if (![responseString isEqualToString:@"Success"]) {
                    return FALSE;
                }
            }
        }
    }
    
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *query = [NSString stringWithFormat:@"DELETE FROM EquipmentQueue"];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
    return TRUE;
}

-(void)updateTractors{
    //TODO: cameron pull everything
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_tractors.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update Tractors responseString: %@",responseString);
    
    NSError *e = nil;
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];

    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO tractors(tractor_id, description) values('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"tractor_id"], [[JSON objectAtIndex:i] objectForKey:@"description"]];
        [self executeQuery:query];
    }
}

-(void)updateEquipment{
    //TODO: cameron pull everything
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_equipment.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update Equipment responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO equipment(equipment_id, description) values('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"equipment_id"], [[JSON objectAtIndex:i] objectForKey:@"description"]];
        [self executeQuery:query];
    }

    
}

-(void)updateCropTypes{
    //TODO: cameron pull everything
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_lookup_crop_types.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update CropTypes responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    NSLog(@"JSON count = %d", [JSON count]);
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_crop_types(crop_id, crop_type) VALUES ('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"crop_id"], [[JSON objectAtIndex:i] objectForKey:@"crop_type"]];
        [self executeQuery:query];
    }
}

-(void)updateLookupOperationMaterials{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_lookup_operation_materials.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update LookupOpMat responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_operation_materials(material_id, operations_materials) values('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"material_id"], [[JSON objectAtIndex:i] objectForKey:@"operations_materials"]];
        [self executeQuery:query];
    }
}

-(void)updateOperationTypes{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_lookup_operation_types.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update LookupOpTest responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_operation_types(type_id, operation_type) values('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"type_id"], [[JSON objectAtIndex:i] objectForKey:@"operation_type"]];
        [self executeQuery:query];
    }
}

-(void)updateOperationUnits{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_lookup_operation_units.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update OpUnits responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO lookup_operation_units(unit_id, operation_units) values('%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"unit_id"], [[JSON objectAtIndex:i] objectForKey:@"operation_units"]];
        [self executeQuery:query];
    }
}

-(void)updateOperationMaterials{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_operation_materials.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update OpMat responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO operation_materials(operation_materials_id, material, description) values('%@','%@','%@');", [[JSON objectAtIndex:i] objectForKey:@"operation_materials_id"], [[JSON objectAtIndex:i] objectForKey:@"material"], [[JSON objectAtIndex:i] objectForKey:@"description"]];
        [self executeQuery:query];
    }
}

-(void)updateSystemNames{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rrproject2.lawr.ucdavis.edu/public/get_system_names.php"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Update SysNames responseString: %@",responseString);
    
    NSArray *JSON = [NSJSONSerialization JSONObjectWithData:response1 options:nil error:nil];
    
    for (int i = 0; i < [JSON count]; i++)
    {
        NSString *query = [NSString stringWithFormat:@"INSERT INTO system_names(system_number, system_name, system_code) values('%@','%@', '%@');", [[JSON objectAtIndex:i] objectForKey:@"system_number"], [[JSON objectAtIndex:i] objectForKey:@"system_name"], [[JSON objectAtIndex:i] objectForKey:@"system_code"]];
        [self executeQuery:query];
    }
}

-(void)addEquipmentQueue
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSMutableArray *returnArray = [[NSMutableArray alloc] init];
        char *error;
        
        NSString *tempName = [self addSlashForQuotes:optionsSingle.equipmentName];
        NSString *tempWidth = [self addSlashForQuotes:optionsSingle.equipmentWidth];
        NSString *tempDesc = [self addSlashForQuotes:optionsSingle.equipmentFinal];
        NSString *tempComm = [self addSlashForQuotes:optionsSingle.equipmentComments];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO EquipmentQueue(equipment_name, width_ft, equipment_description, comments) VALUES (\"%s\", \"%s\", \"%s\", \"%s\");", [tempName UTF8String], [tempWidth UTF8String], [tempDesc UTF8String], [tempComm UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
            
            [returnArray addObject:optionsSingle.tractorFinal];
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else{
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
}

-(void)addTractorQueue
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        NSMutableArray *returnArray = [[NSMutableArray alloc] init];
        char *error;
        
        NSString *tempMake = [self addSlashForQuotes:optionsSingle.tractorMake];
        NSString *tempBrand = [self addSlashForQuotes:optionsSingle.tractorBrand];
        NSString *tempHorse = [self addSlashForQuotes:optionsSingle.tractorHorsepower];
        NSString *tempDesc = [self addSlashForQuotes:optionsSingle.tractorFinal];
        NSString *tempDrive = [self addSlashForQuotes:optionsSingle.tractorDriveTrain];
        NSString *tempFuel = [self addSlashForQuotes:optionsSingle.tractorFuelType];
        NSString *tempComm = [self addSlashForQuotes:optionsSingle.tractorComments];
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO TractorQueue(make, brand, horsepower, description, drive_type, fuel_type, comments) VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\");", [tempMake UTF8String], [tempBrand UTF8String], [tempHorse UTF8String], [tempDesc UTF8String], [tempDrive UTF8String], [tempFuel UTF8String], [tempComm UTF8String]];
        
        NSLog(@"query = %s", [query UTF8String]);
        
        if(sqlite3_exec(database, [query UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Operation");
            
            [returnArray addObject:optionsSingle.tractorFinal];
        }
        else{
            NSLog(@"EXEC ERROR");
        }
    }
    else{
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
}

- (void)removeDB
{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"Operations.sqlite"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    // NSLog(@"path : %@", writableDBPath);
    if (success)
    {
        [fileManager removeItemAtPath:writableDBPath error:&error];
        NSLog(@"This one is the error %@",error);
        return;
    }
}

-(void)getNewDB
{
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        char *error;
        
        NSString *query7 = [NSString stringWithFormat:@"CREATE TABLE sqlite_sequence(name,seq);"];
        
        NSLog(@"query = %s", [query7 UTF8String]);
        
        if(sqlite3_exec(database, [query7 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query0 = [NSString stringWithFormat:@"CREATE TABLE \"PushQueue\" (\"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"date\" text(255,0), \"operation_year\" INTEGER, \"operation_type\" text(255,0), \"equipment_id\" INTEGER, \"equipment_description\" text(255,0), \"tractor_id\" INTEGER, \"tractor_description\" text(255,0), \"operation_description\" text(255,0), \"number_of_passes\" INTEGER, \"number_of_tractor_operators\" INTEGER, \"number_of_workers\" INTEGER, \"operations_comments\" text(255,0), \"tractor_operator\" text(255,0), \"operations_material_id\" INTEGER, \"material\" text(255,0), \"material_description\" text(255,0), \"material_comments\" text(255,0), \"material_quantity\" real, \"material_units\" text(255,0), \"system_number\" INTEGER, \"system_name\" text(255,0), \"system_code\" INTEGER, \"crop_type\" text(255,0), \"operations_materials\" text(255,0));"];
        
        NSLog(@"query = %s", [query0 UTF8String]);
        
        if(sqlite3_exec(database, [query0 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query1 = [NSString stringWithFormat:@"CREATE TABLE \"equipment\" (\"equipment_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"description\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query1 UTF8String]);
        
        if(sqlite3_exec(database, [query1 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query2 = [NSString stringWithFormat:@"CREATE TABLE \"lookup_crop_types\" (\"crop_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"crop_type\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query2 UTF8String]);
        
        if(sqlite3_exec(database, [query2 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query3 = [NSString stringWithFormat:@"CREATE TABLE \"lookup_operation_materials\" (\"material_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"operations_materials\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query3 UTF8String]);
        
        if(sqlite3_exec(database, [query3 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query4 = [NSString stringWithFormat:@"CREATE TABLE \"lookup_operation_types\" (\"type_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"operation_type\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query4 UTF8String]);
        
        if(sqlite3_exec(database, [query4 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query5 = [NSString stringWithFormat:@"CREATE TABLE \"lookup_operation_units\" (\"unit_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"operation_units\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query5 UTF8String]);
        
        if(sqlite3_exec(database, [query5 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query6 = [NSString stringWithFormat:@"CREATE TABLE \"operation_materials\" (\"operation_materials_id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"material\" TEXT(255,0), \"description\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query6 UTF8String]);
        
        if(sqlite3_exec(database, [query6 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query8 = [NSString stringWithFormat:@"CREATE TABLE \"system_names\" (\"system_number\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"system_name\" TEXT(255,0), \"system_code\" TEXT(255,0));"];
        
        NSLog(@"query = %s", [query8 UTF8String]);
        
        if(sqlite3_exec(database, [query8 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query9 = [NSString stringWithFormat:@"CREATE TABLE \"tractors\" (\"tractor_id\" INTEGER NOT NULL, \"description\" TEXT(255,0), PRIMARY KEY(\"tractor_id\"))"];
        
        NSLog(@"query = %s", [query9 UTF8String]);
        
        if(sqlite3_exec(database, [query9 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        NSString *query10 = [NSString stringWithFormat:@"CREATE TABLE \"TractorQueue\" (\"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"make\" text(255,0), \"brand\" text(255,0), \"horsepower\" INTEGER, \"description\" text(255,0), \"drive_type\" text(255,0), \"fuel_type\" text(255,0), \"comments\" text(255,0));"];
        
        NSLog(@"query = %s", [query10 UTF8String]);
        
        if(sqlite3_exec(database, [query10 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
        NSString *query11 = [NSString stringWithFormat:@"CREATE TABLE \"EquipmentQueue\" (\"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"equipment_name\" text(255,0), \"width_ft\" REAL, \"equipment_description\" text(255,0), \"comments\" text(255,0));"];
        
        NSLog(@"query = %s", [query11 UTF8String]);
        
        if(sqlite3_exec(database, [query11 UTF8String], NULL, NULL, &error) == SQLITE_OK)
        {
            NSLog(@"Added Table");
        }
        
        else
        {
            NSLog(@"EXEC ERROR");
        }
        
    }
    else
    {
        NSLog(@"OPEN ERROR");
    }
    sqlite3_close(database);
    
}

- (NSString *) addSlashForQuotes: (NSString *) test
{
    NSLog(@"old string = %@", test);
    
    NSScanner *scanner = [NSScanner scannerWithString:test];
    NSMutableString *germanString = [NSMutableString string];
    BOOL foundQuote = YES;
    int quoteIndex = 0;
    
    while (foundQuote) {
        NSString *nextPart = @"";
        [scanner scanUpToString:@"\"" intoString:&nextPart];
        if (nextPart != nil) {
            [germanString appendString:nextPart];
        }
        foundQuote = [scanner scanString:@"\"" intoString:nil];
        if (foundQuote) {
            [germanString appendString:((quoteIndex % 2) ? @"\"\" " : @"\"\" ")];
            quoteIndex++;
        }
    }
    
    NSLog(@"new string = %@", germanString);
    
    return germanString;
}

-(void)executeQuery:(NSString *)query
{
    //NSLog(@"QUERY : %@",query);
    
    sqlite3_stmt *statement;
    if(sqlite3_open([dbPathString UTF8String], &database) == SQLITE_OK)
    {
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        else
        {
            NSLog(@"query Statement Not Compiled");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    else
    {
        NSLog(@"Data not Opened");
    }
}

- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

@end
