//
//  AddUserLoginCredentialsViewController.h
//  FarmOperations
//
//  Created by Student on 6/3/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUserLoginCredentialsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end
