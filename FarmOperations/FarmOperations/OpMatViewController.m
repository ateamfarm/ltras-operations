//
//  OpMatViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "OpMatViewController.h"
#import "AutocompletionTableView.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface OpMatViewController ()

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UILabel *sampleLabel;
@property (strong, nonatomic) NSArray *array;
@property (strong, nonatomic) NSArray *units;

@end

@implementation OpMatViewController

@synthesize textField = _textField;
@synthesize sampleLabel = _sampleLabel;
@synthesize autoCompleter = _autoCompleter;

- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.textField inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        
        
        NSArray *operationTypesInfo = [[OperationsDatabase database] getAllOpTypes];
        NSMutableArray *opTypeData;
        for(OperationsInfo *info in operationTypesInfo)
        {
            NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsType, nil];
            opTypeData = [NSMutableArray arrayWithArray:opTypeData];
            [opTypeData addObjectsFromArray:temp];
        }
        
        NSArray *temp = [opTypeData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        _autoCompleter.suggestionsDictionary = temp;
    }
    return _autoCompleter;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    
    self.OperationType.text = optionsSingle.opTypeFinal;
    
    //set date to previously set date
    if(optionsSingle.keepDateFinal != nil)
    {
        [self.Date setDate:optionsSingle.keepDateFinal];
    }
    
    [super viewDidLoad];
    // set "value changed" event handler for TextField
    [self.textField addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.OperationType.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if([self shouldPerformSegueWithIdentifier:@"dateToMaterial" sender:self])
    {
        [self performSegueWithIdentifier:@"dateToMaterial" sender:self];
    }
    return NO;
}

- (void)viewDidUnload {
    [self setSampleLabel:nil];
    [self setSampleLabel:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AutoCompleteTableViewDelegate

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    
    NSArray *operationTypesInfo = [[OperationsDatabase database] getAllOpTypes];
    NSMutableArray *opTypeData;
    for(OperationsInfo *info in operationTypesInfo)
    {
        //NSLog(@"%@", info.operationsType);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsType, nil];
        opTypeData = [NSMutableArray arrayWithArray:opTypeData];
        [opTypeData addObjectsFromArray:temp];
    }
    
    NSArray *temp = [opTypeData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return temp;
}

- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    //NSLog(@"%@ - Suggestion chosen: %d", completer, index);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    optionsSingle.opTypeFinal = self.OperationType.text;
    
    //Pull database operations and see if the operation is in the database already.
    NSArray *operationTypesInfo = [[OperationsDatabase database] getAllOpTypes];
    NSMutableArray *opTypeData;
    for(OperationsInfo *info in operationTypesInfo)
    {
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsType, nil];
        opTypeData = [NSMutableArray arrayWithArray:opTypeData];
        [opTypeData addObjectsFromArray:temp];
    }
    NSArray* compare = opTypeData;
    
    //if operation type is new push to database
    BOOL j = false;
    for(int i = 0; i < [compare count]; i++)
    {
        BOOL theyAreEqual = [compare[i] isEqualToString:optionsSingle.opTypeFinal];
        if(theyAreEqual)
        {
            j = true;
        }
    }
    if(j == false)
    {
        [[OperationsDatabase database] addNewOpType];
    }
    
    //format date and year for database
    NSCalendar *cal = [NSCalendar currentCalendar];
    int year = [[cal components:NSYearCalendarUnit fromDate:[self.Date date]] year];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:[self.Date date]];
    
    optionsSingle.yearFinal = [NSString stringWithFormat:@"%d", year];
    optionsSingle.keepDateFinal = [self.Date date];
    
    optionsSingle.dateFinal = stringFromDate;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"dateToMaterial"])
    {
        if([self.OperationType.text length] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Operation Type Error" message:@"You did not insert an operation type." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

@end
