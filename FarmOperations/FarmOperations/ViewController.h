//
//  ViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/14/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"
#import "Reachability.h"

@protocol AutocompletionTableViewDelegate;

@interface ViewController : UIViewController
{
    //Include the global variables
    GlobalVariables *optionsSingle;
    //Include variable to check internet availability
    Reachability *internetReachableFoo;
}

//Holds the keyboard input from the text field for the Tractor
@property (strong, nonatomic) IBOutlet UITextField *Tractor;

//Returns true if connected to internet
//Returns false if not connected to internet
- (BOOL)connected;

@end
