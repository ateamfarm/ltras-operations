//
//  GlobalVariables.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/23/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariables : NSObject

@property(strong, nonatomic) NSString *tractorFinal;
@property(strong, nonatomic) NSString *equipmentFinal;
@property(strong, nonatomic) NSString *opTypeFinal;
@property(strong, nonatomic) NSString *matTypeFinal;
@property(strong, nonatomic) NSString *matDescFinal;
@property(strong, nonatomic) NSString *matCommFinal;
@property(strong, nonatomic) NSString *matUnitsFinal;
@property(strong, nonatomic) NSString *matUnitTypeFinal;
@property(strong, nonatomic) NSString *dateFinal;
@property(strong, nonatomic) NSString *yearFinal;
@property(strong, nonatomic) NSString *systemFinal;
@property(strong, nonatomic) NSString *cropFinal;
@property(strong, nonatomic) NSString *tracOpFinal;
@property(strong, nonatomic) NSString *tracOperatorFinal;
@property(strong, nonatomic) NSString *workFinal;
@property(strong, nonatomic) NSString *passFinal;
@property(strong, nonatomic) NSString *descFinal;
@property(strong, nonatomic) NSString *commFinal;
@property(strong, nonatomic) NSDate *keepDateFinal;
@property(strong, nonatomic) NSString *systemCode;
@property(strong, nonatomic) NSString *tractorMake;
@property(strong, nonatomic) NSString *tractorBrand;
@property(strong, nonatomic) NSString *tractorHorsepower;
@property(strong, nonatomic) NSString *tractorDriveTrain;
@property(strong, nonatomic) NSString *tractorFuelType;
@property(strong, nonatomic) NSString *tractorComments;
@property(strong, nonatomic) NSString *equipmentName;
@property(strong, nonatomic) NSString *equipmentWidth;
@property(strong, nonatomic) NSString *equipmentComments;

+(GlobalVariables *) singleObj;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) init;

@end
