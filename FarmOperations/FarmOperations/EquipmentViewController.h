//
//  EquipmentViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 5/6/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"
#import "Reachability.h"


@interface EquipmentViewController : UIViewController
{
    GlobalVariables *optionsSingle;
    Reachability *internetReachableFoo;
}

@property (weak, nonatomic) IBOutlet UITextField *Name;
@property (weak, nonatomic) IBOutlet UITextField *Width;
@property (strong, nonatomic) IBOutlet UITextField *Description;
@property (weak, nonatomic) IBOutlet UITextView *Comments;

- (BOOL)connected;

@end
