//
//  TractorWebViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 5/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "TractorWebViewController.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface TractorWebViewController ()

@end

@implementation TractorWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.username.delegate = self;
    self.password.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _username)
    {
        [self.password becomeFirstResponder];
    }
    else if(textField == _password)
    {
        if([self shouldPerformSegueWithIdentifier:@"LoginTractor" sender:self])
        {
            [self performSegueWithIdentifier:@"LoginTractor" sender:self];
        }
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
      
    [[OperationsDatabase database] addNewTractor];
    
    if([optionsSingle.tractorComments isEqualToString:@""])
    {
        optionsSingle.tractorComments = [NSString stringWithFormat:@" "];
    }
    
    
    NSString *postString = [NSString stringWithFormat:@"TractorMake=%@&TractorBrand=%@&TractorHorsepower=%d&TractorDescription=%@&TractorDriveType=%@&TractorFuelType=%@&TractorComments=%@", optionsSingle.tractorMake, optionsSingle.tractorBrand, [optionsSingle.tractorHorsepower intValue], optionsSingle.tractorFinal, optionsSingle.tractorDriveTrain, optionsSingle.tractorFuelType, optionsSingle.tractorComments];
    postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
    NSLog(@"PostString:%@", postString);
    
    NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [NSString stringWithFormat:@"http://russellranchoperations.lawr.ucdavis.edu/public/add_tractor.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:data];
    NSLog(@"request:%@", request);
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
    
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<!DOCTYPE html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</html>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"<body>" withString:@""];
    responseString = [responseString stringByReplacingOccurrencesOfString:@"</body>" withString:@""];
    
    response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"responseString: %@",responseString);
    //this if statement will be true if a success response was given by the server
    //ANDREW: if this statement is false, throw a pop to the user saying the input wasn't successful
    if (![responseString isEqualToString:@"Success"]) {
        NSLog(@"ResponseString was = to Success");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"Tractor was not added because of a server error." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    optionsSingle.tractorMake = nil;
    optionsSingle.tractorHorsepower = nil;
    optionsSingle.tractorFuelType = nil;
    optionsSingle.tractorDriveTrain = nil;
    optionsSingle.tractorComments = nil;
    optionsSingle.tractorBrand = nil;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"LoginTractor"])
    {
        NSString *md5_pass = [[OperationsDatabase database] md5:_password.text];
        NSLog(@"md5_pass = %@", md5_pass);
        
        
        NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", _username.text, md5_pass];
        postString = [postString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        NSLog(@"Login PostString:%@", postString);
        
        postString = [postString stringByReplacingOccurrencesOfString:@"=&" withString:@"=+&"];
        postString = [postString stringByReplacingOccurrencesOfString:@"" withString:@"+"];
        
        NSData *data = [postString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *str = [NSString stringWithFormat:@"http://rrproject2.lawr.ucdavis.edu/public/login.php"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [data length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:data];

        
        NSLog(@"request:%@", request);
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        
        NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:response1 encoding:NSUTF8StringEncoding];
        
        response1 = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"Login responseString: %@",responseString);
        
        
        //NSLog(@"responseString: %@",responseString);
        if (![responseString isEqualToString:@"Success"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Unsuccessful" message:@"You entered your email or password incorrectly." delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        
    }
    return YES;

}


@end
