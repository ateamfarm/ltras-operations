//
//  FillViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "FillViewController.h"

@interface FillViewController ()

@end

@implementation FillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    
    self.TracOperators.text = optionsSingle.tracOpFinal;
    self.Workers.text = optionsSingle.workFinal;
    self.Passes.text = optionsSingle.passFinal;
    self.Description.text = optionsSingle.descFinal;
    self.Comments.text = optionsSingle.commFinal;
    self.tractorOperator.text = optionsSingle.tracOperatorFinal;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    _Comments.layer.borderWidth = 1.5f;
//    _Comments.layer.borderColor = [[UIColor blueColor]CGColor];
//    _Description.layer.borderWidth = 1.5f;
//    _Description.layer.borderColor = [[UIColor greenColor]CGColor];
    
    //NSLog(@"tractor = %@", optionsSingle.tractorFinal);
    
    self.TracOperators.delegate = self;
    self.Workers.delegate = self;
    self.Passes.delegate = self;
    self.tractorOperator.delegate = self;
    self.Description.delegate = self;
    self.Comments.delegate = self;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqual:@"\n"] && (textView == _Description))
    {
        [_Comments becomeFirstResponder];
        return NO;
    }
    else if ([text isEqual:@"\n"] && (textView == _Comments))
    {
        [textView resignFirstResponder];
        if([self shouldPerformSegueWithIdentifier:@"submitFill" sender:self])
        {
            [self performSegueWithIdentifier:@"submitFill" sender:self];
        }
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == _TracOperators)
    {
        [_Workers becomeFirstResponder];
    }
    else if(textField == _Workers)
    {
        [_Passes becomeFirstResponder];
    }
    else if(textField == _Passes)
    {
        [_tractorOperator becomeFirstResponder];
    }
    else if(textField == _tractorOperator)
    {
        [_Description becomeFirstResponder];
    }
    return NO;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    optionsSingle.tracOpFinal = self.TracOperators.text;
    optionsSingle.workFinal = self.Workers.text;
    optionsSingle.passFinal = self.Passes.text;
    optionsSingle.tracOperatorFinal = self.tractorOperator.text;
    
    if([optionsSingle.tracOpFinal length] == 0)
    {
        optionsSingle.tracOpFinal = @"1";
    }
    if([optionsSingle.workFinal length] == 0)
    {
        optionsSingle.workFinal = @"0";
    }
    if([optionsSingle.passFinal length] == 0)
    {
        optionsSingle.passFinal = @"1";
    }
    if([optionsSingle.tracOperatorFinal length] == 0)
    {
        optionsSingle.tracOperatorFinal = @"Operator IH";
    }
    
    optionsSingle.descFinal = self.Description.text;
    optionsSingle.commFinal = self.Comments.text;
        //NSLog(@"tractor=%@, ", optionsSingle.tractorFinal);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    optionsSingle.tracOpFinal = self.TracOperators.text;
    optionsSingle.workFinal = self.Workers.text;
    optionsSingle.passFinal = self.Passes.text;
    
    if([optionsSingle.tracOpFinal length] == 0)
    {
        optionsSingle.tracOpFinal = @"1";
    }
    if([optionsSingle.workFinal length] == 0)
    {
        optionsSingle.workFinal = @"0";
    }
    if([optionsSingle.passFinal length] == 0)
    {
        optionsSingle.passFinal = @"1";
    }
    if([optionsSingle.tracOperatorFinal length] == 0)
    {
        optionsSingle.tracOperatorFinal = @"Operator IH";
    }
    
    if([identifier isEqualToString:@"submitFill"])
    {
        if(([optionsSingle.tractorFinal length] == 0) || ([optionsSingle.tracOpFinal intValue] == 0 && (![optionsSingle.tracOpFinal isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Number of Operators Error" message:@"Please enter an integer value for the number of tractor operators" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(([optionsSingle.workFinal length] == 0) || ([optionsSingle.workFinal intValue] == 0 && (![optionsSingle.workFinal isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Number of Workers Error" message:@"Please enter an integer value for the number of workers" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        if(([optionsSingle.passFinal length] == 0) || ([optionsSingle.passFinal intValue] == 0 && (![optionsSingle.passFinal isEqual: @"0"])))
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Number of Passes Error" message:@"Please enter an integer value for the number of passes" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}


@end
