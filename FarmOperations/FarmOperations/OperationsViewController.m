//
//  OperationsViewController.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/24/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "OperationsViewController.h"
#import "SubmitCell.h"

@interface OperationsViewController ()

@end

@implementation OperationsViewController
@synthesize opDate, opSystem, opTractor, opID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    //OPTIONAL TODO: Andrew Add edit swipe button
    optionsSingle = [GlobalVariables singleObj];
    [super viewDidLoad];
    
    NSArray *operationDate = [[OperationsDatabase database] getAllDates];
    NSMutableArray *dateData;
    for(OperationsInfo *info in operationDate)
    {
        //NSLog(@"%@", info.operationsSystem);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.finalDates, nil];
        dateData = [NSMutableArray arrayWithArray:dateData];
        [dateData addObjectsFromArray:temp];
    }
    opDate = dateData;
    NSLog(@"OPDATE = %@", opDate);
    
    
    NSArray *operationSystems = [[OperationsDatabase database] getAllFinalSystems];
    NSMutableArray *systemsData;
    for(OperationsInfo *info in operationSystems)
    {
        //NSLog(@"%@", info.operationsSystem);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.finalSystems, nil];
        systemsData = [NSMutableArray arrayWithArray:systemsData];
        [systemsData addObjectsFromArray:temp];
    }
    opSystem = systemsData;
    NSLog(@"OPSYS = %@", opSystem);
    
    NSArray *operationTractor = [[OperationsDatabase database] getAllFinalTractors];
    NSMutableArray *tractorData;
    for(OperationsInfo *info in operationTractor)
    {
        //NSLog(@"%@", info.operationsSystem);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.finalTractors, nil];
        tractorData = [NSMutableArray arrayWithArray:tractorData];
        [tractorData addObjectsFromArray:temp];
    }
    opTractor = tractorData;
    NSLog(@"OPTRAC = %@", opTractor);
	// Do any additional setup after loading the view.
    
    NSArray *operationID = [[OperationsDatabase database] getAllFinalID];
    NSMutableArray *IDData;
    for(OperationsInfo *info in operationID)
    {
        //NSLog(@"%@", info.operationsSystem);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.finalID, nil];
        IDData = [NSMutableArray arrayWithArray:IDData];
        [IDData addObjectsFromArray:temp];
    }
    opID = IDData;
    NSLog(@"OPTRAC = %@", opID);


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return opSystem.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"SubmitCell";
    
    SubmitCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[SubmitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    int row = [indexPath row];
    
    NSLog(@"%@", opTractor[row]);
    
    cell.Date.text = opDate[row];
    cell.System.text = opSystem[row];
    cell.Tractor.text = opTractor[row];
    
    return cell;
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //add code here for when you hit delete
    // Remove the row from data model
    
    //add code to remove database entry at ID
    NSMutableArray* temp = [opID objectAtIndex:indexPath.row];
    
    [[OperationsDatabase database] deleteByID:temp];
    
    [opDate removeObjectAtIndex:indexPath.row];
    [opSystem removeObjectAtIndex:indexPath.row];
    [opTractor removeObjectAtIndex:indexPath.row];
    
    // Request table view to reload
    [tableView reloadData];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"NewOperation"])
    {
        optionsSingle.tractorFinal = nil;
        optionsSingle.equipmentFinal = nil;
        optionsSingle.opTypeFinal = nil;
        optionsSingle.matTypeFinal = nil;
        optionsSingle.matDescFinal = nil;
        optionsSingle.matUnitsFinal = nil;
        optionsSingle.matUnitTypeFinal = nil;
        optionsSingle.dateFinal = nil;
        optionsSingle.yearFinal = nil;
        optionsSingle.systemFinal = nil;
        optionsSingle.tracOpFinal = nil;
        optionsSingle.workFinal = nil;
        optionsSingle.passFinal = nil;
        optionsSingle.descFinal = nil;
        optionsSingle.commFinal = nil;
        optionsSingle.keepDateFinal = nil;
        optionsSingle.cropFinal = nil;
        optionsSingle.systemCode = nil;
        optionsSingle.tractorMake = nil;
        optionsSingle.tractorBrand = nil;
        optionsSingle.tractorHorsepower = nil;
        optionsSingle.tractorDriveTrain = nil;
        optionsSingle.tractorFuelType = nil;
        optionsSingle.tractorComments = nil;
        optionsSingle.equipmentName = nil;
        optionsSingle.equipmentWidth = nil;
        optionsSingle.equipmentComments = nil;
        optionsSingle.tracOperatorFinal = nil;

    }

}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"Submit"])
    {
        if(![self connected])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Error" message:@"You do not have an internet connection. Please wait until you have a connection to submit this operation." delegate:self cancelButtonTitle:@"Back" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
