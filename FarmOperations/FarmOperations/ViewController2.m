//
//  ViewController2.m
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import "ViewController2.h"
#import "AutocompletionTableView.h"
#import "OperationsInfo.h"
#import "OperationsDatabase.h"

@interface ViewController2 ()

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UILabel *sampleLabel;

@end

@implementation ViewController2

@synthesize textField = _textField;
@synthesize sampleLabel = _sampleLabel;
@synthesize autoCompleter = _autoCompleter;


- (AutocompletionTableView *)autoCompleter
{
    if (!_autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:YES] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        _autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.textField inViewController:self withOptions:options];
        _autoCompleter.autoCompleteDelegate = self;
        
        
        NSArray *operationEquipmentInfo = [[OperationsDatabase database] getAllEquipment];
        NSMutableArray *equipmentData;
        for(OperationsInfo *info in operationEquipmentInfo)
        {
            //NSLog(@"%@", info.operationsEquipment);
            NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsEquipment, nil];
            equipmentData = [NSMutableArray arrayWithArray:equipmentData];
            [equipmentData addObjectsFromArray:temp];
        }
        
        NSArray *temp = [equipmentData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        _autoCompleter.suggestionsDictionary = temp;
    }
    return _autoCompleter;
}

- (void)viewDidLoad
{
    optionsSingle = [GlobalVariables singleObj];
    self.Equipment.text = optionsSingle.equipmentFinal;
    //NSLog(@"in Second: tractor = %@", [optionsSingle tractorFinal]);
    
    [super viewDidLoad];
    
    // set "value changed" event handler for TextField
    [self.textField addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    self.Equipment.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if([self shouldPerformSegueWithIdentifier:@"EquipmentToOperation" sender:self])
    {
        [self performSegueWithIdentifier:@"EquipmentToOperation" sender:self];
    }
    return NO;
}

- (void)viewDidUnload {
    [self setSampleLabel:nil];
    [self setSampleLabel:nil];
    [super viewDidUnload];
}

#pragma mark - AutoCompleteTableViewDelegate

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    
    NSArray *operationEquipmentInfo = [[OperationsDatabase database] getAllEquipment];
    NSMutableArray *equipmentData;
    for(OperationsInfo *info in operationEquipmentInfo)
    {
        //NSLog(@"%@", info.operationsEquipment);
        NSMutableArray *temp = [NSMutableArray arrayWithObjects:info.operationsEquipment, nil];
        equipmentData = [NSMutableArray arrayWithArray:equipmentData];
        [equipmentData addObjectsFromArray:temp];
    }
    
    NSArray *temp = [equipmentData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return temp;
}



- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // invoked when an available suggestion is selected
    //NSLog(@"%@ - Suggestion chosen: %d", completer, index);
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    optionsSingle.equipmentFinal = self.Equipment.text;
//NSLog(@"tractor=%@, ", optionsSingle.tractorFinal);
    //NSLog(@"equipment=%@, ", optionsSingle.equipmentFinal);
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    optionsSingle.equipmentFinal = self.Equipment.text;
    int error = [[OperationsDatabase database] getEquipmentID];
    
    if([identifier isEqualToString:@"EquipmentToOperation"])
    {
        if(error < 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Equipment Error" message:@"Equipment not found in the database" delegate:self cancelButtonTitle:@"Back" otherButtonTitles:@"Add Equipment", nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self performSegueWithIdentifier:@"AddEquipment" sender:self];
    }
}



@end
