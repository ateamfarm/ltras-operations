//
//  FillViewController.h
//  FarmOperations
//
//  Created by Andrew McAllister on 4/16/14.
//  Copyright (c) 2014 LTRAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalVariables.h"

@interface FillViewController : UIViewController
{
    GlobalVariables *optionsSingle;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *Comments;
@property (strong, nonatomic) IBOutlet UITextView *Description;
@property (strong, nonatomic) IBOutlet UITextField *TracOperators;
@property (strong, nonatomic) IBOutlet UITextField *Workers;
@property (strong, nonatomic) IBOutlet UITextField *Passes;
@property (strong, nonatomic) IBOutlet UITextField *tractorOperator;


@end
